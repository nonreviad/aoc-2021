fn roll_dice(dice: u32) -> u32 {
    let dice = (dice + 1) % 100;
    if dice == 0 {
        return 100;
    }
    dice
}

fn game(player1: u32, player2: u32) -> u32 {
    let mut player1 = player1;
    let mut player2 = player2;
    let mut dice = 1;
    let mut first_player_turn = true;
    let mut score1 = 0;
    let mut score2 = 0;
    let mut rolls = 1;
    loop {
        if first_player_turn {
            for _ in 0..3 {
                player1 += dice;
                rolls += 1;
                dice = roll_dice(dice);
            }
            player1 %= 10;
            if player1 == 0 {
                player1 = 10;
            }
            score1 += player1;
        } else {
            for _ in 0..3 {
                player2 += dice;
                rolls += 1;
                dice = roll_dice(dice);
            }
            player2 %= 10;
            if player2 == 0 {
                player2 = 10;
            }
            score2 += player2;
        }
        first_player_turn = !first_player_turn;
        if score1 >= 1000 {
            return score2 * (rolls - 1);
        }
        if score2 >= 1000 {
            return score1 * (rolls - 1);
        }
    }
}

struct State {
    position: u8,
    score: u8,
}

fn evolve_states(states: Vec<State>) -> (Vec<State>, usize) {
    let mut wins = 0;
    let mut next_states: Vec<State> = Vec::new();
    for state in states.into_iter() {
        for r1 in 1..=3 {
            for r2 in 1..=3 {
                for r3 in 1..=3 {
                    let roll = r1 + r2 + r3;
                    let mut position = (state.position + roll) % 10;
                    if position == 0 {
                        position = 10;
                    }
                    let score = state.score + position;
                    if score >= 21 {
                        wins += 1;
                    } else {
                        next_states.push(State { position, score });
                    }
                }
            }
        }
    }
    return (next_states, wins);
}

fn dirac_game(player1: u8, player2: u8) -> (usize, usize) {
    let mut states_1: Vec<State> = Vec::new();
    let mut states_2: Vec<State> = Vec::new();
    states_1.push(State {
        position: player1,
        score: 0,
    });
    states_2.push(State {
        position: player2,
        score: 0,
    });
    let mut ways_1 = 0;
    let mut ways_2 = 0;
    while !states_1.is_empty() || !states_2.is_empty() {
        let (next_states, wins) = evolve_states(states_1);
        ways_1 += wins * states_2.len();
        states_1 = next_states;
        let (next_states, wins) = evolve_states(states_2);
        ways_2 += wins * states_1.len();
        states_2 = next_states;
    }
    (ways_1, ways_2)
}
#[test]
fn test_part_one() {
    assert_eq!(game(4, 8), 739785);
}

#[test]
fn test_part_two() {
    assert_eq!(dirac_game(4, 8), (444356092776315, 341960390180808));
}

fn main() {
    println!("{}", game(9, 10));
    println!("{:?}", dirac_game(9, 10));
}