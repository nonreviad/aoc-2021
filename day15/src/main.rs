use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

struct Cave {
    board: Vec<Vec<u8>>,
}

#[derive(Eq, PartialEq)]
struct PathNode {
    position: (usize, usize),
    distance: u32,
}

impl Ord for PathNode {
    fn cmp(&self, other: &Self) -> Ordering {
        other.distance.cmp(&self.distance)
    }
}

impl PartialOrd for PathNode {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(other.distance.cmp(&self.distance))
    }
}


#[derive(Eq, PartialEq)]
struct AStarPathNode {
    position: (usize, usize),
    f: u32,
    g: u32,
}

impl Ord for AStarPathNode {
    fn cmp(&self, other: &Self) -> Ordering {
        other.f.cmp(&self.f)
    }
}

impl PartialOrd for AStarPathNode {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(other.f.cmp(&self.f))
    }
}

impl Cave {
    fn from_file(filename: &str) -> Self {
        let file = File::open(filename).unwrap();
        let reader = BufReader::new(file);
        let board: Vec<Vec<u8>> = reader
            .lines()
            .map(|x| {
                let r: Vec<u8> = x.unwrap().trim().chars().map(|c| (c as u8) - 48).collect();
                r
            })
            .collect();
        Cave { board }
    }
    fn min_path_dijkstra(self: &Self) -> u32 {
        let num_rows = self.board.len();
        let num_cols = self.board[0].len();
        let end = (num_rows - 1, num_cols - 1);
        let mut dists: HashMap<(usize, usize), u32> = HashMap::new();
        let mut q: BinaryHeap<PathNode> = BinaryHeap::new();
        let deviations = [(0, 1), (2, 1), (1, 0), (1, 2)];
        q.push(PathNode {
            position: (0,0),
            distance: 0
        });
        dists.insert((0,0), 0);
        loop {
            if let Some(path_node) = q.pop() {
                if path_node.position == end {
                    continue;
                }
                for (drow, dcol) in deviations {
                    let row = path_node.position.0 + drow;
                    let col = path_node.position.1 + dcol;
                    if row == 0 || col == 0 || row > num_rows || col > num_cols {
                        continue;
                    }
                    let row = row - 1;
                    let col = col - 1;
                    if path_node.distance > dists[&path_node.position] {
                        continue;
                    }
                    let new_dist = path_node.distance + self.board[row][col] as u32;
                    let old_dist = dists.entry((row, col)).or_insert(u32::MAX);
                    if *old_dist > new_dist {
                        *old_dist = new_dist;
                        q.push(PathNode {
                            position: (row, col),
                            distance: new_dist,
                        });
                    }
                }
            } else {
                break;
            }
        }
        *dists.get(&end).unwrap()
    }
    fn min_path_a_star(self: &Self) -> u32 {
        let real_num_rows = self.board.len();
        let real_num_cols = self.board[0].len();
        let num_rows = real_num_rows * 5;
        let num_cols = real_num_cols * 5;
        let end = (num_rows - 1, num_cols - 1);
        let deviations = [(0, 1), (2, 1), (1, 0), (1, 2)];
        let mut open_set: BinaryHeap<AStarPathNode> = BinaryHeap::new();
        open_set.push(AStarPathNode {
            position: (0,0),
            f: 0,
            g: 0
        });
        let mut dists: HashMap<(usize, usize), u32> = HashMap::new();
        dists.insert((0,0), 0);
        let mut open_nodes: HashSet<(usize, usize)> = HashSet::new();
        open_nodes.insert((0,0));
        let mut closed_nodes: HashSet<(usize, usize)> = HashSet::new();
        loop {
            if let Some(path_node) = open_set.pop() {
                let mut path_node = path_node;
                path_node.g = *dists.get(&path_node.position).unwrap();
                open_nodes.remove(&path_node.position);
                closed_nodes.insert(path_node.position);
                if path_node.position == end {
                    return path_node.g;
                }
                for (drow, dcol) in deviations {
                    let row = path_node.position.0 + drow;
                    let col = path_node.position.1 + dcol;
                    if row == 0 || col == 0 || row > num_rows || col > num_cols {
                        continue;
                    }
                    let row = row - 1;
                    let col = col - 1;
                    let position = (row, col);
                    if closed_nodes.contains(&position) {
                        continue;
                    }
                    let real_row = row % real_num_rows;
                    let real_col = col % real_num_cols;
                    let real_board = self.board[real_row][real_col] as u32
                        + (row / real_num_rows + col / real_num_cols) as u32;
                    let mut real_board = real_board % 9;
                    if real_board == 0 {
                        real_board = 9;
                    }
                    let new_g = path_node.g + real_board;
                    let old_g = dists.entry(position).or_insert(u32::MAX);
                    if new_g < *old_g {
                        *old_g = new_g;
                        let h = (num_rows - row + num_cols - col - 2) as u32;
                        open_nodes.insert(position);
                        open_set.push(AStarPathNode {
                            position: position,
                            f: new_g + h,
                            g: new_g,
                        });
                    }
                }
            } else {
                break;
            }
        }
        panic!("Oh noooo");
    }
}

fn part_one(filename: &str) -> u32 {
    let cave = Cave::from_file(filename);
    cave.min_path_dijkstra()
}

fn part_two(filename: &str) -> u32 {
    let cave = Cave::from_file(filename);
    cave.min_path_a_star()
}

#[test]
fn test_smol_part_one() {
    assert_eq!(part_one("input_smol.txt"), 40);
}

#[test]
fn test_smol_part_two() {
    assert_eq!(part_two("input_smol.txt"), 315);
}

#[test]
fn test_part_one() {
    assert_eq!(part_one("input.txt"), 714);
}

#[test]
fn test_part_two() {
    assert_eq!(part_two("input.txt"), 2948);
}

fn main() {
    println!("{}", part_one("input.txt"));
    println!("{}", part_two("input.txt"));
}
