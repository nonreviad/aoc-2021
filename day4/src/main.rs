use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

struct BingoBoard {
    board: Vec<u32>,
    marked: Vec<bool>,
    final_score: u32,
    finished: bool,
}

impl BingoBoard {
    fn new(board: Vec<u32>) -> Self {
        if board.len() != 25 {
            panic!("Wrong board size! {}", board.len());
        }
        BingoBoard {
            board: board,
            marked: vec![false; 25],
            final_score: 0,
            finished: false,
        }
    }
    fn from_lines(lines: &mut std::io::Lines<BufReader<File>>) -> Option<Self> {
        let mut line: String = String::new();
        while line.len() == 0 {
            match lines.next() {
                Some(l) => line = String::from(l.unwrap().trim()),
                _ => return None,
            }
        }
        let mut board_lines: Vec<u32> = Vec::new();
        for _ in 0..5 {
            line.split(' ').for_each(|x| {
                if x.len() > 0 {
                    let num: u32 = x.parse::<u32>().unwrap();
                    board_lines.push(num);
                }
            });
            if let Some(l) = lines.next() {
                line = String::from(l.unwrap().trim());
            }
        }
        Some(BingoBoard::new(board_lines))
    }
    fn pos_marked(self: &Self, row: usize, col: usize) -> bool {
        self.marked[row * 5 + col]
    }
    fn check_done(self: &Self) -> bool {
        for row in 0..5 {
            let mut won = true;
            for col in 0..5 {
                if !self.pos_marked(row, col) {
                    won = false;
                    break;
                }
            }
            if won {
                return true;
            }
        }
        for col in 0..5 {
            let mut won = true;
            for row in 0..5 {
                if !self.pos_marked(row, col) {
                    won = false;
                    break;
                }
            }
            if won {
                return true;
            }
        }
        false
    }
    fn try_bingo(self: &mut Self, number: u32) {
        if self.finished {
            return;
        }
        for i in 0..25 {
            if self.board[i] == number {
                self.marked[i] = true;
                if self.check_done() {
                    self.finished = true;
                    self.final_score = self
                        .board
                        .iter()
                        .zip(self.marked.iter())
                        .map(|(board_number, visited)| -> u32 {
                            if *visited {
                                return 0;
                            } else {
                                return *board_number;
                            }
                        })
                        .sum();
                    self.final_score *= number;
                }
            }
        }
    }
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);
    let mut lines = reader.lines();
    let numbers: Vec<u32> = lines
        .next()
        .unwrap()
        .unwrap()
        .split(',')
        .map(|x: &str| x.parse::<u32>().unwrap())
        .collect();
    let mut boards: Vec<BingoBoard> = Vec::new();
    loop {
        if let Some(board) = BingoBoard::from_lines(&mut lines) {
            boards.push(board);
        } else {
            break;
        }
    }
    let mut found_first: bool = false;
    let mut last_score: u32 = 0;
    for number in numbers.iter() {
        for board in boards.iter_mut() {
            if !board.finished {
                board.try_bingo(*number);
                if board.finished {
                    if !found_first {
                        println!("First score: {}", board.final_score);
                        last_score = board.final_score;
                        found_first = true;
                    } else {
                        last_score = board.final_score;
                    }
                }
            }
        }
    }
    println!("Last score: {}", last_score);
    Ok(())
}
