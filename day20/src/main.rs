use itertools::Itertools;
use std::collections::HashSet;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

#[derive(Debug, PartialEq, Eq, Hash)]
struct Point {
    x: i32,
    y: i32,
}
fn neighbors(position: &Point) -> Vec<Point> {
    (-1..=1)
        .cartesian_product(-1..=1)
        .map(|(x, y)| Point {
            x: x + position.x,
            y: y + position.y,
        })
        .collect()
}
fn evolve(
    num_rows: i32,
    num_cols: i32,
    state: &HashSet<Point>,
    mappings: &Vec<bool>,
) -> HashSet<Point> {
    let ret = (-500..=num_cols + 500)
        .cartesian_product(-500..=num_rows + 500)
        .map(|(x, y)| Point { x, y })
        .filter(|p| {
            let idx = neighbors(p)
                .into_iter()
                .map(|n| {
                    if state.contains(&n) {
                        return 1;
                    }
                    return 0;
                })
                .fold(0, |acc, x| acc * 2 + x);
            mappings[idx]
        })
        .collect();
    ret
}
fn read_points(filename: &str) -> HashSet<Point> {
    let file = File::open(filename).unwrap();
    let reader = BufReader::new(file);
    reader
        .lines()
        .enumerate()
        .flat_map(|(x, line)| {
            let line = line.unwrap();
            line.chars()
                .enumerate()
                .map(|(y, c)| (x as i32, y as i32, c == '#'))
                .collect::<Vec<_>>()
        })
        .filter(|(_, _, f)| *f)
        .map(|(x, y, _)| Point { x, y })
        .collect()
}

#[test]
fn test_smol() {
    let enhancement: Vec<bool> = "..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#"
        .chars().map(|c| c == '#').collect();
    let points = read_points("input_smol.txt");
    let first_step = evolve(15, 15, &points, &enhancement);
    let second_step = evolve(15, 15, &first_step, &enhancement);
    assert_eq!(
        second_step
            .iter()
            .filter(|p| p.x >= 0 && p.y >= 0 && p.x < 15 && p.y < 15)
            .count(),
        35
    );
}

#[test]
fn test_lorge() {
    let enhancement: Vec<bool> = "..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#"
        .chars().map(|c| c == '#').collect();
    let mut points = read_points("input_smol.txt");
    for _ in 0..50 {
        points = evolve(15, 15, &points, &enhancement);
    }
    assert_eq!(
        points
            .iter()
            .count(),
        3351
    );
}

fn main() {
    let enhancement: Vec<bool> = "##.....##.#.#####.#...###...#.##..#....##..#.##.#.#....##.....#.##.##.#.#.#...#.#.#.###.##..#.#.#.#..#.##.#...#..#.#.#..#####.##.#..#..##.#..#.#...#.....#.###..#..#####.##...#..##..##...#.#...##.##..##...##.##.#......#...##.##.#####.#....####....######.#.#.......#.############.###..#..#......####......#..##.####.##....#..#.#.###..#.####.####.#.##.##.##..###.#..#.......#....#..########....##..##.#...#.#.###.###.###..#..#.###..#....#.###..#.##.##..###.#.#####....###.##.###.....#######........#.#.##...##.#...."
        .chars().map(|c| c == '#').collect();

    let mut points = read_points("input.txt");
    for _ in 0..50 {
        points = evolve(100, 100, &points, &enhancement);
    }
    println!(
        "{}",
        points
            .iter()
            .filter(|p| p.x >= -300 && p.y >= -300 && p.x < 400 && p.y < 400)
            .count()
    );
}
