use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::collections::VecDeque;
use std::collections::HashMap;
use std::collections::HashSet;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
enum Cave {
    None,
    Start,
    End,
    SmallCave(String),
    BigCave(String)
}

impl Cave {
    fn new(s: &str) -> Self {
        match s {
            "start" => return Cave::Start,
            "end" => return Cave::End,
            _ => {
                let c: char = s.chars().next().unwrap();
                if c.is_lowercase() {
                    return Cave::SmallCave(String::from(s));
                } else {
                    return Cave::BigCave(String::from(s));
                }
            }
        }
    }
}

struct CaveSystem {
    adjacency_list: HashMap<Cave, Vec<Cave>>
}

impl CaveSystem {
    fn new() -> Self {
        CaveSystem {
            adjacency_list: HashMap::new()
        }
    }
    fn add_path(self: &mut Self, from_cave:Cave, to_cave: Cave) {
        if !self.adjacency_list.contains_key(&from_cave) {
            self.adjacency_list.insert(from_cave.clone(), Vec::new());
        }
        if !self.adjacency_list.contains_key(&to_cave) {
            self.adjacency_list.insert(to_cave.clone(), Vec::new());
        }
        self.adjacency_list.get_mut(&to_cave).unwrap().push(from_cave.clone());
        self.adjacency_list.get_mut(&from_cave).unwrap().push(to_cave.clone());
    }
}

struct Path {
    visited: HashSet<Cave>,
    so_far: Vec<Cave>,
    visited_twice: Cave,
    current_node: Cave
}

impl Path {
    fn new() -> Self {
        Path {
            visited: HashSet::new(),
            so_far: vec![Cave::Start],
            current_node: Cave::Start,
            visited_twice: Cave::None
        }
    }
    fn fork(self: &Self, new_node: &Cave) -> Self {
        let mut new_visited = self.visited.clone();
        if let Cave::SmallCave(_) = new_node {
            new_visited.insert(new_node.clone());
        }
        let mut so_far = self.so_far.clone();
        so_far.push(new_node.clone());
        Path {
            visited: new_visited,
            so_far: so_far,
            current_node: new_node.clone(),
            visited_twice: self.visited_twice.clone()
        }
    }
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);
    let mut cave_system = CaveSystem::new();
    reader.lines().for_each(|line| {
        let line = line.unwrap();
        let caves: Vec<&str> = line.split('-').collect();
        let cave_from = Cave::new(caves[0]);
        let cave_to = Cave::new(caves[1]);
        cave_system.add_path(cave_from, cave_to);
    });
    let mut ways: u32 = 0;
    let mut queue: VecDeque<Path> = VecDeque::new();
    queue.push_back(Path::new());
    loop {
        if let Some(p) = queue.pop_front() {
            if let Some(neighbors) = cave_system.adjacency_list.get(&p.current_node) {
                for neighbor in neighbors.iter() {
                    match neighbor {
                        Cave::End => {
                            ways += 1;
                        },
                        Cave::BigCave(_) | Cave::SmallCave(_) => {
                            if !p.visited.contains(neighbor) {
                                queue.push_back(p.fork(neighbor));
                            }
                        },
                        _ => {
                            // Do nothing
                        }
                    }
                }
            }
        } else {
            break
        }
    }
    println!("{}", ways);
    let mut ways: u32 = 0;
    let mut queue: VecDeque<Path> = VecDeque::new();
    queue.push_back(Path::new());
    loop {
        if let Some(p) = queue.pop_front() {
            if let Some(neighbors) = cave_system.adjacency_list.get(&p.current_node) {
                for neighbor in neighbors.iter() {
                    match neighbor {
                        Cave::End => {
                            ways += 1;
                        },
                        Cave::SmallCave(_) => {
                            if !p.visited.contains(neighbor) {
                                queue.push_back(p.fork(neighbor));
                            } else if let Cave::None = p.visited_twice {
                                let mut p2 = p.fork(neighbor);
                                p2.visited_twice = neighbor.clone();
                                queue.push_back(p2);
                            }
                        },
                        Cave::BigCave(_) => {
                            queue.push_back(p.fork(neighbor));
                        }
                        _ => {
                            // Do nothing
                        }
                    }
                }
            }
        } else {
            break
        }
    }
    println!("{}", ways);
    Ok(())
}