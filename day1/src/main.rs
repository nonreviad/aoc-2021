use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

fn main() {
    if let Ok(lines) = read_lines("input.txt") {
        let mut depths: Vec<i32> = Vec::new();
        for line in lines {
            depths.push(line.unwrap().parse().unwrap());
        }
        let mut increased = 0;
        for i in 1..depths.len() {
            if depths[i] > depths[i - 1] {
                increased += 1;
            }
        }
        println!("{}", increased);
        let mut prev_window = depths[0] + depths[1] + depths[2];
        let mut increased_windows = 0;
        for i in 3..depths.len() {
            let curr_window = prev_window - depths[i-3] + depths[i];
            if curr_window > prev_window {
                increased_windows += 1;
            }
            prev_window = curr_window;
        }
        println!("{}", increased_windows);
    }
}
