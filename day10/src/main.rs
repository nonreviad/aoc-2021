use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

use std::collections::VecDeque;

fn invalid_score(c: char) -> u64 {
    match c {
        ')' => return 3,
        ']' => return 57,
        '}' => return 1197,
        '>' => return 25137,
        _ => panic!("What!")
    }
}

fn invalid_chunk_score(s: &str) -> u64 {
    let mut stack: VecDeque<char> = VecDeque::new();
    for c in s.chars() {
        match c {
            ')' | ']' | '}' | '>' => {
                if let Some(before) = stack.pop_back() {
                    match (before, c) {
                        ('(',')') | ('[',']') | ('{','}') | ('<','>') => {},
                        _ => return invalid_score(c)
                    }
                } else {
                    return invalid_score(c);
                }
            },
            _ => stack.push_back(c)
        }
    }
    0
}

fn complete_score(c: char) -> u64 {
    match c {
        '(' => return 1,
        '[' => return 2,
        '{' => return 3,
        '<' => return 4,
        _ => panic!("What!")
    }
}

fn complete_chunk_score(s: &str) -> u64 {
    let mut score: u64 = 0;
    let mut stack: VecDeque<char> = VecDeque::new();
    for c in s.chars() {
        match c {
            ')' | ']' | '}' | '>' => {
                stack.pop_back();
            },
            _ => stack.push_back(c)
        }
    }
    loop {
        if let Some(c) = stack.pop_back() {
            score = score * 5 + complete_score(c);
        } else {
            break;
        }
    }
    score
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);
    let chunks: Vec<String> = reader.lines().map(|x| x.unwrap()).collect();
    let part_one_score: u64 = chunks.iter().map(|x| invalid_chunk_score(x)).sum();
    println!("{}", part_one_score);
    let mut part_two_scores: Vec<u64> = chunks.iter()
        .filter(|s| invalid_chunk_score(s) == 0)
        .map(|s| complete_chunk_score(s))
        .collect();
    part_two_scores.sort();
    println!("{}", part_two_scores[part_two_scores.len() / 2]);
    Ok(())
}
