use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::collections::BTreeMap;

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let mut reader = BufReader::new(file);
    let mut line: String = String::new();
    let _ = reader.read_line(&mut line);
    let crab_positions: Vec<i64> = line.split(',')
        .map(|x: &str| x.parse::<i64>().unwrap())
        .collect();
    // Number of crabs at a given position, sorted by x coordinate
    let mut sorted_crab_positions: BTreeMap<i64, i64> = BTreeMap::new();
    for position in crab_positions.iter() {
        let num = sorted_crab_positions.entry(*position).or_insert(0);
        *num += 1;
    }
    let min_x: i64 = *sorted_crab_positions.keys().min().unwrap();
    let max_x: i64 = *sorted_crab_positions.keys().max().unwrap();
    // Part 1
    let mut best_fuel_cost = i64::MAX;
    for x in min_x..=max_x {
        let mut cost = 0;
        for (other_x, count) in sorted_crab_positions.iter() {
            cost += (x - other_x).abs() * count;
        }
        if cost < best_fuel_cost {
            best_fuel_cost = cost;
        }
    }
    println!("{}", best_fuel_cost);
    // Part 2
    let mut best_fuel_cost = i64::MAX;
    for x in min_x..=max_x {
        let mut cost = 0;
        for (other_x, count) in sorted_crab_positions.iter() {
            let dist = (x - other_x).abs();
            let dist = dist * (dist + 1) / 2;
            cost += dist * count;
        }
        if cost < best_fuel_cost {
            best_fuel_cost = cost;
        }
    }
    println!("{}", best_fuel_cost);
    Ok(())
}

