use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

struct Point {
    row: i32,
    column: i32
}

struct Line {
    from_point: Point,
    to_point: Point
}

impl Line {
    fn new(l1: i32, c1: i32, l2: i32, c2: i32) -> Self {
        Line {
            from_point: Point {
                row: l1,
                column: c1
            },
            to_point: Point {
                row: l2,
                column: c2
            }
        }
    }
}
struct Board {
    max_c: i32,
    board: Vec<u32>
}
impl Board {
    fn new(max_l: i32, max_c: i32) -> Self {
        Board {
            max_c: max_c,
            board: vec![0; (max_l * max_c) as usize]
        }
    }
    fn part_one(self: &mut Self, v: &Line) {
        let from_l = v.from_point.row;
        let to_l = v.to_point.row;
        let from_c = v.from_point.column;
        let to_c = v.to_point.column;
        let dl = (to_l - from_l).signum();
        let dc = (to_c - from_c).signum();
        let mut l = from_l;
        let mut c = from_c;
        if dl != 0 && dc != 0 {
            return;
        }
        loop {
            let idx = (l * self.max_c + c) as usize;
            self.board[idx] += 1;
            if l == to_l && c == to_c {
                break
            }
            l += dl;
            c += dc;
        }
    }
    fn part_two(self: &mut Self, v: &Line) {
        let from_l = v.from_point.row;
        let to_l = v.to_point.row;
        let from_c = v.from_point.column;
        let to_c = v.to_point.column;
        let dl = (to_l - from_l).signum();
        let dc = (to_c - from_c).signum();
        let mut l = from_l;
        let mut c = from_c;
        if dl != 0 && dc != 0 && (from_l - to_l).abs() != (from_c - to_c).abs() {
            return;
        }
        loop {
            let idx = (l * self.max_c + c) as usize;
            self.board[idx] += 1;
            if l == to_l && c == to_c {
                break
            }
            l += dl;
            c += dc;
        }
    }
}
fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);
    let mut lines: Vec<Line> = Vec::new();
    let mut max_l = 0;
    let mut max_c = 0;
    for line in reader.lines() {
        let line = line.unwrap();
        let parts: Vec<&str> = line.split(" -> ").collect();
        let p1: Vec<&str> = parts[0].split(',').collect();
        let p2: Vec<&str> = parts[1].split(',').collect();
        let c1 = p1[1].parse::<i32>().unwrap();
        let l1 = p1[0].parse::<i32>().unwrap();
        let c2 = p2[1].parse::<i32>().unwrap();
        let l2 = p2[0].parse::<i32>().unwrap();
        max_l = std::cmp::max(max_l, l1);
        max_l = std::cmp::max(max_l, l2);
        max_c = std::cmp::max(max_c, c1);
        max_c = std::cmp::max(max_c, c2);
        lines.push(Line::new(l1, c1, l2, c2));
    }
    max_l += 1;
    max_c += 1;
    let mut board = Board::new(max_l, max_c);
    for line in lines.iter() {
        board.part_one(line);
    }
    let part1 = board.board.iter().filter(|x| **x >= 2).count();
    println!("{}", part1);

    let mut board = Board::new(max_l, max_c);
    for line in lines.iter() {
        board.part_two(line);
    }
    let part2 = board.board.iter().filter(|x| **x >= 2).count();
    println!("{}", part2);
    Ok(())
}
