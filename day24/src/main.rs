use std::fs::File;
use std::hash::Hash;
use std::io::prelude::*;
use std::io::BufReader;
use std::collections::HashMap;
use std::collections::VecDeque;
enum Register {
    X,
    Y,
    Z,
    W
}

impl Register {
    fn from_str(s: &str) -> Option<Register> {
        match s {
            "x" => Some(Register::X),
            "y" => Some(Register::Y),
            "z" => Some(Register::Z),
            "w" => Some(Register::W),
            _ => None
        }
    }
}

enum Operand {
    Reg(Register),
    Val(i64)
}

impl Operand {
    fn from_str(s: &str) -> Operand {
        if let Some(reg) = Register::from_str(s) {
            Operand::Reg(reg)
        } else {
            Operand::Val(s.parse::<i64>().unwrap())
        }
    }
}

enum Operation {
    Input(Register),
    Add(Register, Operand),
    Mul(Register, Operand),
    Div(Register, Operand),
    Mod(Register, Operand),
    Eql(Register, Operand)
}

impl Operation {
    fn from_line(s: &str) -> Operation {
        let parts: Vec<&str> = s.split(" ").collect();
        match parts[0] {
            "inp" => Operation::Input(Register::from_str(parts[1]).unwrap()),
            "add" => Operation::Add(Register::from_str(parts[1]).unwrap(), Operand::from_str(parts[2])),
            "mul" => Operation::Mul(Register::from_str(parts[1]).unwrap(), Operand::from_str(parts[2])),
            "div" => Operation::Div(Register::from_str(parts[1]).unwrap(), Operand::from_str(parts[2])),
            "mod" => Operation::Mod(Register::from_str(parts[1]).unwrap(), Operand::from_str(parts[2])),
            "eql" => Operation::Eql(Register::from_str(parts[1]).unwrap(), Operand::from_str(parts[2])),
            _ => panic!("Unknown operation {}", s)
        }
    }
}
#[derive(Debug, Clone, Hash, PartialEq, Eq)]
struct ALU {
    x: i64,
    y: i64,
    z: i64,
    w: i64
}

enum EvalError {
    DivByZero,
    ModError
}

impl ALU {
    fn new() -> Self {
        Self {
            x: 0,
            y: 0,
            z: 0,
            w: 0
        }
    }
    fn set_val(&mut self, reg: &Register, val: i64) {
        match reg {
            Register::X => self.x = val,
            Register::Y => self.y = val,
            Register::Z => self.z = val,
            Register::W => self.w = val,
        }
    }
    fn get_reg_val(&self, reg: &Register) -> i64 {
        match reg {
            Register::X => self.x,
            Register::Y => self.y,
            Register::Z => self.z,
            Register::W => self.w,
        }
    }

    fn get_val(&self, operand: &Operand) -> i64 {
        match operand {
            Operand::Reg(reg) => self.get_reg_val(reg),
            Operand::Val(val) => *val
        }
    }
    fn eval_op(&mut self, operation: &Operation) -> Result<(), EvalError> {
        match operation {
            Operation::Add(reg, operand) => {
                self.set_val(reg, self.get_reg_val(reg) + self.get_val(operand));
            },
            Operation::Mul(reg, operand) => {
                self.set_val(reg, self.get_reg_val(reg) * self.get_val(operand));
            },
            Operation::Div(reg, operand) => {
                let b = self.get_val(operand);
                if b == 0 {
                    return Err(EvalError::DivByZero);
                }
                self.set_val(reg, self.get_reg_val(reg) / b);
            },
            Operation::Mod(reg, operand) => {
                let a = self.get_reg_val(reg);
                let b = self.get_val(operand);
                if a < 0 || b <= 0 {
                    return Err(EvalError::ModError);
                }
                self.set_val(reg, a % b);
            },
            Operation::Eql(reg, operand) => {
                if self.get_reg_val(reg) == self.get_val(operand) {
                    self.set_val(reg, 1);
                } else {
                    self.set_val(reg, 0);
                }
            },
            Operation::Input(_) => panic!("Should handle separately!")
        }
        Ok(())
    }
}
fn part_one(operations_list: &Vec<Vec<Operation>>) {
    let mut to_explore: HashMap<ALU, i64> = HashMap::new();
    to_explore.insert(ALU::new(), 0);
    for operations in operations_list.iter() {
        let mut new_states: HashMap<ALU, i64> = HashMap::new();
        for (alu, val) in to_explore.iter() {
            for i in 1..=9 {
                let new_val = val * 10 + i;
                let mut new_alu = alu.clone();
                new_alu.set_val(&Register::W, i);
                for operation in operations.iter() {
                    let _ = new_alu.eval_op(operation);
                }
                if let Some(x) = new_states.get_mut(&new_alu) {
                    let old_val = *x;
                    if new_val > old_val {
                        *x = new_val;
                    }
                } else {
                    new_states.insert(new_alu, new_val);
                }
            }
        }
        to_explore = new_states;
    }
    let mut max_val = 0;
    for (alu, val) in to_explore.iter() {
        if alu.z == 0 {
            if *val > max_val {
                max_val = *val;
            }
        }
    }
    println!("{}", max_val);
}

fn part_two(operations_list: &Vec<Vec<Operation>>) {
    let mut to_explore: HashMap<ALU, i64> = HashMap::new();
    to_explore.insert(ALU::new(), 0);
    for operations in operations_list.iter() {
        let mut new_states: HashMap<ALU, i64> = HashMap::new();
        for (alu, val) in to_explore.iter() {
            for i in 1..=9 {
                let new_val = val * 10 + i;
                let mut new_alu = alu.clone();
                new_alu.set_val(&Register::W, i);
                for operation in operations.iter() {
                    let _ = new_alu.eval_op(operation);
                }
                if let Some(x) = new_states.get_mut(&new_alu) {
                    let old_val = *x;
                    if new_val < old_val {
                        *x = new_val;
                    }
                } else {
                    new_states.insert(new_alu, new_val);
                }
            }
        }
        to_explore = new_states;
    }
    let mut min_val = i64::MAX;
    for (alu, val) in to_explore.iter() {
        if alu.z == 0 {
            if *val < min_val {
                min_val = *val;
            }
        }
    }
    println!("{}", min_val);
}

fn main() {
    let file = File::open("input.txt").unwrap();
    let reader = BufReader::new(file);
    let commands: Vec<Operation> = reader.lines().map(|line| {
        let line = line.unwrap();
        let line = line.trim();
        Operation::from_line(line)
    }).collect();
    let mut command_commands: VecDeque<Vec<Operation>> = VecDeque::new();
    command_commands.push_back(Vec::new());
    commands.into_iter().for_each(|command| {
        if let Operation::Input(_) = command {
            command_commands.push_back(Vec::new());
        } else {
            let idx = command_commands.len() - 1;
            command_commands[idx].push(command);
        }
    });
    command_commands.pop_front();
    let command_commands = command_commands.into_iter().collect();
    part_one(&command_commands);
    part_two(&command_commands);
}
