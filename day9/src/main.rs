use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

use std::collections::HashSet;
use std::collections::VecDeque;

struct Cavern {
    num_rows: usize,
    num_cols: usize,
    floor: Vec<u32>
}

impl Cavern {
    fn new(num_rows: usize, num_cols: usize, floor: Vec<u32>) -> Self {
        Cavern {
            num_rows: num_rows,
            num_cols: num_cols,
            floor: floor
        }
    }
    fn get_neighbors(self: &Self, pos: usize) -> Vec<usize> {
        let mut neighbors: Vec<usize> = Vec::new();
        let row = pos / self.num_cols;
        let col = pos % self.num_cols;
        for d_row in 0..=2 {
            for d_col in 0..=2 {
                if d_row != d_col && d_row != (2 - d_col) {
                    let new_row = row + d_row;
                    let new_col = col + d_col;
                    if new_row >= 1 && new_row <= self.num_rows && new_col >= 1 && new_col <= self.num_cols {
                        let new_row = new_row - 1;
                        let new_col = new_col - 1;
                        neighbors.push(new_row * self.num_cols + new_col);
                    }
                }
            }
        }
        neighbors
    }
    fn gen_low_points(self: &Self) -> Vec<usize> {
        let mut result: Vec<usize> = Vec::new();
        for row in 0..self.num_rows {
            for col in 0..self.num_cols {
                let pos = row * self.num_cols + col;
                let val = self.floor[pos];
                let low_point: bool = self.get_neighbors(pos)
                    .iter()
                    .all(|new_pos| val < self.floor[*new_pos]);
                if  low_point {
                    result.push(row * self.num_cols + col);
                }
            }
        }
        result
    }
    fn part1(self: &Self) -> u32 {
        return self.gen_low_points().iter().map(|x| self.floor[*x] + 1).sum();   
    }
    fn part2(self: &Self) -> usize {
        let mut basin_sizes: Vec<usize> = self.gen_low_points().iter()
            .map(|pos| {
                let mut to_visit: VecDeque<usize> = VecDeque::new();
                let mut marked: HashSet<usize> = HashSet::new();
                to_visit.push_back(*pos);
                marked.insert(*pos);
                loop {
                    if let Some(node) = to_visit.pop_front() {
                        self.get_neighbors(node).iter().for_each(|other: &usize| {
                            let val = self.floor[*other];
                            if val != 9 && !marked.contains(other) {
                                marked.insert(*other);
                                to_visit.push_back(*other);
                            }
                        });
                    } else {
                        break;
                    }
                }
                marked.len()
            })
            .collect();
        basin_sizes.sort_by(|x, y| y.cmp(x));
        return basin_sizes[0] * basin_sizes[1] * basin_sizes[2];
    }
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);
    let floor: Vec<String> = reader.lines()
        .map(|line| String::from(line.unwrap().trim()))
        .collect();
    let num_rows = floor.len();
    let num_cols = floor[0].len();
    let floor: String = floor.join("");
    let floor: Vec<u32> = floor.chars().map(|c: char| ((c as u8) - b'0') as u32).collect();
    let cavern = Cavern::new(num_rows, num_cols, floor);
    println!("{}", cavern.part1());
    println!("{}", cavern.part2());
    Ok(())
}
