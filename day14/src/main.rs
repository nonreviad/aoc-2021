use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::collections::HashMap;

type Freq = HashMap<char, i64>;

fn inc_freq(freq: &mut Freq, c: char, val: i64) {
    let new_freq: i64 = if let Some(x) = freq.get(&c) {*x + val} else {val};
    freq.insert(c, new_freq);
}

fn merge_freqs(freq1: &Freq, freq2: &Freq) -> Freq {
    let mut freq: Freq = freq1.clone();
    for (k, v) in freq2.iter() {
        inc_freq(&mut freq, *k, *v);
    }
    freq
}

fn get_template_score(freqs: &HashMap<(char, char), Freq>, template: &str) -> i64 {
    let mut freq: Freq = Freq::new();
    let empty_freq = Freq::new();
    for i in 0..template.len() - 1 {
        let a: char = template.chars().nth(i).unwrap();
        let b: char = template.chars().nth(i + 1).unwrap();
        freq = merge_freqs(&freq, freqs.get(&(a,b)).unwrap_or_else(|| &empty_freq));
        if i > 0 {
            inc_freq(&mut freq, a, -1);
        }
    }
    let min_freq: i64 = *freq.values().min().unwrap_or(&0);
    let max_freq: i64 = *freq.values().max().unwrap_or(&0);
    max_freq - min_freq
}


fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let mut reader = BufReader::new(file);

    let mut line: String = String::new();
    let _ = reader.read_line(&mut line);
    let template = String::from(line.trim());

    line.clear();
    let _ = reader.read_line(&mut line);
    let mut rules: HashMap<(char, char), char> = HashMap::new();
    let mut freqs: HashMap<(char, char), Freq> = HashMap::new();
    loop {
        line.clear();
        let sz = reader.read_line(&mut line);
        if let Ok(0) = sz {
            break;
        }
        let s = line.trim();
        let (pattern, insert) = s.split_once(" -> ").unwrap();
        let mut freq: Freq = Freq::new();
        let insert = insert.chars().nth(0).unwrap();
        inc_freq(&mut freq, insert, 1);
        let pair = (pattern.chars().nth(0).unwrap(), pattern.chars().nth(1).unwrap());
        inc_freq(&mut freq, pair.0, 1);
        inc_freq(&mut freq, pair.1, 1);
        freqs.insert(pair, freq);
        rules.insert(pair, insert);
    }
    
    for i in 1..40 {
        let mut new_freqs: HashMap<(char, char), Freq> = HashMap::new();
        for ((a, b), v) in rules.iter() {
            let f1 = freqs.get(&(*a,*v)).unwrap();
            let f2 = freqs.get(&(*v,*b)).unwrap();
            let mut f = merge_freqs(f1, f2);
            inc_freq(&mut f, *v, -1);
            new_freqs.insert((*a,*b), f);
        }
        freqs = new_freqs;
        if i == 9 || i == 39 {
            println!("{}", get_template_score(&freqs, &template));
        }
    }
    Ok(())
}