use std::collections::VecDeque;
use std::collections::HashSet;
use std::cmp::min;
use std::cmp::max;
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
struct State {
    cost_so_far: usize,
    a_stack: Vec<char>,
    b_stack: Vec<char>,
    c_stack: Vec<char>,
    d_stack: Vec<char>,
    stack_size: usize,
    top: [char;11]
}

fn cost_per_distance(c: char)-> usize {
    match c {
        'a' => 1,
        'b' => 10,
        'c' => 100,
        'd' => 1000,
        _ => panic!("Invalid char: {}", c)
    }
}

impl State {
    fn is_final(&self) -> bool {
        self.a_stack.iter().all(|x| *x == 'a') &&
        self.b_stack.iter().all(|x| *x == 'b') &&
        self.c_stack.iter().all(|x| *x == 'c') &&
        self.d_stack.iter().all(|x| *x == 'd') &&
        self.top.iter().all(|x| *x == ' ')
    }
    fn best_cost_solution(&self) -> usize {
        let mut q: VecDeque<State> = VecDeque::new();
        let mut visited_states: HashSet<State> = HashSet::new();
        q.push_back(self.clone());
        let mut best_cost = usize::MAX;
        loop {
            if let Some(state) = q.pop_front() {
                if visited_states.contains(&state) {
                    continue;
                }
                visited_states.insert(state.clone());
                if state.is_final() {
                    if state.cost_so_far < best_cost {
                        best_cost = state.cost_so_far;
                    }
                } else {
                    let v = state.new_states();
                    for s in v.into_iter() {
                        q.push_back(s);
                    }
                }
            } else {
                break;
            }
        }
        best_cost
    }
    fn new_states(&self) -> Vec<State> {
        let mut others: Vec<State> = Vec::new();
        if self.a_stack.len() > 0 && !self.a_stack.iter().all(|x| *x == 'a') {
            let top = self.a_stack[self.a_stack.len() - 1];
            for idx in 0..11 {
                if idx == 2 || idx == 4 || idx == 6 || idx == 8 {
                    continue;
                }
                let lft = min(idx, 2);
                let rgt = max(idx, 2);
                if (lft..=rgt).all(|i|self.top[i] == ' ') {
                    let cost = (self.stack_size - self.a_stack.len() + 1 + rgt - lft) * cost_per_distance(top);
                    let mut new_state = self.clone();
                    new_state.a_stack.pop();
                    new_state.top[idx] = top;
                    new_state.cost_so_far += cost;
                    others.push(new_state);
                }
            }
        }
        if self.b_stack.len() > 0 && !self.b_stack.iter().all(|x| *x == 'b') {
            let top = self.b_stack[self.b_stack.len() - 1];
            for idx in 0..11 {
                if idx == 2 || idx == 4 || idx == 6 || idx == 8 {
                    continue;
                }
                let lft = min(idx, 4);
                let rgt = max(idx, 4);
                if (lft..=rgt).all(|i|self.top[i] == ' ') {
                    let cost = (self.stack_size - self.b_stack.len() + 1 + rgt - lft) * cost_per_distance(top);
                    let mut new_state = self.clone();
                    new_state.b_stack.pop();
                    new_state.top[idx] = top;
                    new_state.cost_so_far += cost;
                    others.push(new_state);
                }
            }
        }
        if self.c_stack.len() > 0 && !self.c_stack.iter().all(|x| *x == 'c') {
            let top = self.c_stack[self.c_stack.len() - 1];
            for idx in 0..11 {
                if idx == 2 || idx == 4 || idx == 6 || idx == 8 {
                    continue;
                }
                let lft = min(idx, 6);
                let rgt = max(idx, 6);
                if (lft..=rgt).all(|i|self.top[i] == ' ') {
                    let cost = (self.stack_size - self.c_stack.len() + 1 + rgt - lft) * cost_per_distance(top);
                    let mut new_state = self.clone();
                    new_state.c_stack.pop();
                    new_state.top[idx] = top;
                    new_state.cost_so_far += cost;
                    others.push(new_state);
                }
            }
        }
        if self.d_stack.len() > 0 && !self.d_stack.iter().all(|x| *x == 'd') {
            let top = self.d_stack[self.d_stack.len() - 1];
            for idx in 0..11 {
                if idx == 2 || idx == 4 || idx == 6 || idx == 8 {
                    continue;
                }
                let lft = min(idx, 8);
                let rgt = max(idx, 8);
                if (lft..=rgt).all(|i|self.top[i] == ' ') {
                    let cost = (self.stack_size - self.d_stack.len() + 1 + rgt - lft) * cost_per_distance(top);
                    let mut new_state = self.clone();
                    new_state.d_stack.pop();
                    new_state.top[idx] = top;
                    new_state.cost_so_far += cost;
                    others.push(new_state);
                }
            }
        }
        for idx in 0..11 {
            if self.top[idx] == ' ' {
                continue;
            }
            match self.top[idx] {
                'a' => {
                    if self.a_stack.len() == self.stack_size || !self.a_stack.iter().all(|x| *x == 'a') {
                        continue;
                    }
                    let lft = min(2,idx);
                    let rgt = max(2,idx);
                    if (lft..=rgt).all(|i| i == idx || self.top[i] == ' ') {
                        let cost = (self.stack_size - self.a_stack.len() + rgt - lft) * cost_per_distance('a');
                        let mut new_state = self.clone();
                        new_state.top[idx] = ' ';
                        new_state.a_stack.push('a');
                        new_state.cost_so_far += cost;
                        others.push(new_state);
                    }
                },
                'b' => {
                    if self.b_stack.len() == self.stack_size || !self.b_stack.iter().all(|x| *x == 'b') {
                        continue;
                    }
                    let lft = min(4,idx);
                    let rgt = max(4,idx);
                    if (lft..=rgt).all(|i| i == idx || self.top[i] == ' ') {
                        let cost = (self.stack_size - self.b_stack.len() + rgt - lft) * cost_per_distance('b');
                        let mut new_state = self.clone();
                        new_state.top[idx] = ' ';
                        new_state.b_stack.push('b');
                        new_state.cost_so_far += cost;
                        others.push(new_state);
                    }
                },
                'c' => {
                    if self.c_stack.len() == self.stack_size || !self.c_stack.iter().all(|x| *x == 'c') {
                        continue;
                    }
                    let lft = min(6,idx);
                    let rgt = max(6,idx);
                    if (lft..=rgt).all(|i| i == idx || self.top[i] == ' ') {
                        let cost = (self.stack_size - self.c_stack.len() + rgt - lft) * cost_per_distance('c');
                        let mut new_state = self.clone();
                        new_state.top[idx] = ' ';
                        new_state.c_stack.push('c');
                        new_state.cost_so_far += cost;
                        others.push(new_state);
                    }
                },
                'd' => {
                    if self.d_stack.len() == self.stack_size || !self.d_stack.iter().all(|x| *x == 'd') {
                        continue;
                    }
                    let lft = min(8,idx);
                    let rgt = max(8,idx);
                    if (lft..=rgt).all(|i| i == idx || self.top[i] == ' ') {
                        let cost = (self.stack_size - self.d_stack.len() + rgt - lft) * cost_per_distance('d');
                        let mut new_state = self.clone();
                        new_state.top[idx] = ' ';
                        new_state.d_stack.push('d');
                        new_state.cost_so_far += cost;
                        others.push(new_state);
                    }
                },
                _ => panic!("Invalid char")
            }
        }
        others
    }
}

#[test]
fn test_part_one() {
    let initial_state = State {
        a_stack: vec!['a', 'b'],
        b_stack: vec!['d', 'c'],
        c_stack: vec!['c', 'b'],
        d_stack: vec!['a', 'd'],
        stack_size: 2,
        top: [' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '],
        cost_so_far: 0
    };
    assert_eq!(initial_state.best_cost_solution(), 12521);
}

#[test]
fn test_part_two() {
    let initial_state = State {
        a_stack: vec!['a', 'd', 'd', 'b'],
        b_stack: vec!['d', 'b', 'c', 'c'],
        c_stack: vec!['c', 'a', 'b', 'b',],
        d_stack: vec!['a', 'c', 'a', 'd'],
        stack_size: 4,
        top: [' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '],
        cost_so_far: 0
    };
    assert_eq!(initial_state.best_cost_solution(), 44169);
}
fn main() {
    let initial_state = State {
        a_stack: vec!['b', 'c'],
        b_stack: vec!['c', 'b'],
        c_stack: vec!['a', 'd'],
        d_stack: vec!['a', 'd'],
        stack_size: 4,
        top: [' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '],
        cost_so_far: 0
    };
    println!("{}",initial_state.best_cost_solution());

    let initial_state = State {
        a_stack: vec!['b', 'd', 'd', 'c'],
        b_stack: vec!['c', 'b', 'c', 'b'],
        c_stack: vec!['a', 'a', 'b', 'd'],
        d_stack: vec!['a', 'c', 'a', 'd'],
        stack_size: 4,
        top: [' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '],
        cost_so_far: 0
    };
    println!("{}",initial_state.best_cost_solution());
}
