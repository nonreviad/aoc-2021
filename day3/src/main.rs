use std::io::BufReader;
use std::io::prelude::*;
use std::fs::File;
#[derive(Debug)]
struct Trie {
    prefix: String,
    leaf: bool,
    count_one: u32,
    count_zero: u32,
    zero: Option<Box<Trie>>,
    one: Option<Box<Trie>>
}

impl Trie {
    fn new() -> Self {
        Trie {
            prefix: String::from(""),
            leaf: false,
            count_one: 0,
            count_zero: 0,
            zero: Option::None,
            one: Option::None
        }
    }
    fn push(self: &mut Self, num: &str) {
        match num.chars().nth(0) {
            Some('1') => {
                self.count_one += 1;
                if self.one.is_none() {
                    let mut new_box = Box::new(Trie::new());
                    new_box.prefix = String::from(&self.prefix) + "1";
                    self.one = Some(new_box);
                }
                match &mut self.one {
                    Some(one) => {
                        one.push(&num[1..]);
                    },
                    None => {
                        panic!("Could not allocate :(")
                    }
                }
            },
            Some('0') => {
                self.count_zero += 1;
                if self.zero.is_none() {
                    let mut new_box = Box::new(Trie::new());
                    new_box.prefix = String::from(&self.prefix) + "0";
                    self.zero = Some(new_box);
                }
                match &mut self.zero {
                    Some(zero) => {
                        zero.push(&num[1..]);
                    },
                    None => {
                        panic!("Could not allocate :(")
                    }
                }
            },
            Some(c) => {
                panic!("Invalid character {}", c);
            }
            None => {
                self.leaf = true;
            }
        }   
    }
    fn oxygen(self: &Self) -> &String {
        if self.leaf {
            return &self.prefix;
        }
        if self.count_one >= self.count_zero {
            match &self.one {
                Some(one) => {
                    return one.oxygen();
                },
                _ => {
                    panic!("");
                }
            }
        }
        match &self.zero {
            Some(zero) => {
                return zero.oxygen();
            },
            _ => {
                panic!("");
            }
        }
    }
    fn carbon_dioxide(self: &Self) -> &String {
        if self.leaf {
            return &self.prefix;
        }
        if (self.count_zero > 0 && (self.count_zero <= self.count_one)) || self.count_one == 0 {
            match &self.zero {
                Some(zero) => {
                    return zero.carbon_dioxide();
                },
                _ => {
                    panic!("Oh no :(");
                }
            }
        }
        match &self.one {
            Some(one) => {
                return one.carbon_dioxide();
            },
            _ => {
                panic!("Oh no 2 :(");
            }
        }
    }
}

fn main() -> std::io::Result<()> {
    let f = File::open("input.txt")?;
    let reader = BufReader::new(f);
    let mut counts: Vec<u32> = Vec::new();
    let mut numbers: Vec<String> = Vec::new();
    let mut trie = Trie::new();
    reader.lines().for_each(|line| {
        let line = line.unwrap();
        trie.push(&line);
        if counts.is_empty() {
            counts.resize(line.len(), 0);
        }
        for (idx, ch) in line.chars().enumerate() {
            if ch == '1' {
                counts[idx] += 1;
            }
        }
        numbers.push(line);
    });
    // Part 1
    let mut gamma: u32 = 0;
    let mut epsilon: u32 = 0;
    let threshold: u32 = (numbers.len() as u32) / 2;
    for c in counts {
        gamma *= 2;
        epsilon *= 2;
        if c > threshold {
            gamma += 1;
        } else {
            epsilon += 1;
        }
    }
    println!("{}", gamma * epsilon);
    // Part 2
    let o2 = u32::from_str_radix(trie.oxygen(), 2).unwrap();
    let co2 = u32::from_str_radix(trie.carbon_dioxide(), 2).unwrap();
    println!("{} {} {}", o2, co2, o2*co2);
    Ok(())
}
