use std::collections::VecDeque;

#[derive(Debug)]
enum Bit {
    One,
    Zero
}

fn char_to_iter(c: char) -> [Bit; 4] {
    match c {
        '0' => [Bit::Zero, Bit::Zero, Bit::Zero, Bit::Zero],
        '1' => [Bit::Zero, Bit::Zero, Bit::Zero, Bit::One],
        '2' => [Bit::Zero, Bit::Zero, Bit::One, Bit::Zero],
        '3' => [Bit::Zero, Bit::Zero, Bit::One, Bit::One],
        '4' => [Bit::Zero, Bit::One, Bit::Zero, Bit::Zero],
        '5' => [Bit::Zero, Bit::One, Bit::Zero, Bit::One],
        '6' => [Bit::Zero, Bit::One, Bit::One, Bit::Zero],
        '7' => [Bit::Zero, Bit::One, Bit::One, Bit::One],
        '8' => [Bit::One, Bit::Zero, Bit::Zero, Bit::Zero],
        '9' => [Bit::One, Bit::Zero, Bit::Zero, Bit::One],
        'A' => [Bit::One, Bit::Zero, Bit::One, Bit::Zero],
        'B' => [Bit::One, Bit::Zero, Bit::One, Bit::One],
        'C' => [Bit::One, Bit::One, Bit::Zero, Bit::Zero],
        'D' => [Bit::One, Bit::One, Bit::Zero, Bit::One],
        'E' => [Bit::One, Bit::One, Bit::One, Bit::Zero],
        'F' => [Bit::One, Bit::One, Bit::One, Bit::One],
        _ => panic!("Invalid character {}", c)
    }
}

fn string_to_iter(s: &str) -> Vec<Bit> {
    s.chars().flat_map(|x| char_to_iter(x)).collect()
}

fn consume(strim: &[Bit], num_bits: usize) -> (usize, &[Bit]) {
    let ret: usize = strim[0..num_bits].iter().fold(0, |accum, x| {
        match x {
            Bit::One => accum * 2 + 1,
            Bit::Zero => accum * 2
        }
    });
    (ret, &strim[num_bits..])
}

#[derive(Debug)]
enum OperatorType {
    Sum,
    Product,
    Minimum,
    Maximum,
    GreaterThan,
    LessThan,
    EqualTo
}

#[derive(Debug)]
enum PacketPayload {
    LiteralValue(usize),
    Operator(OperatorType, Vec<Packet>)
}

#[derive(Debug)]
struct Packet {
    version: usize,
    payload: PacketPayload
}

fn extract_value(strim: &[Bit]) ->(usize, &[Bit]) {
    let mut value = 0;
    let mut sstrim = strim;
    loop {
        let (should_continue, strim) = consume(sstrim, 1);
        sstrim = strim;
        let (next_nibble, strim) = consume(sstrim, 4);
        sstrim = strim;
        value = value * 16 + next_nibble;
        if should_continue == 0 {
            return (value, sstrim)
        }
    }
}
impl Packet {
    fn new(strim: & [Bit]) -> (Self, &[Bit]) {
        let (version, strim) = consume(strim, 3);
        let (packet_type, strim) = consume(strim, 3);
        match packet_type {
            4 => {
                let (value, strim) = extract_value(strim);
                (Packet{
                    version,
                    payload: PacketPayload::LiteralValue(value)
                }, strim)
            },
            _ => {
                let (length_type, strim) = consume(strim, 1);
                let mut v: Vec<Packet> = Vec::new();
                let mut sstrim = strim;
                if length_type == 0 {
                    let (length_value, strim) = consume(sstrim, 15);
                    sstrim = &strim[length_value..];
                    let strim = &strim[..length_value];
                    let mut ssstrim = strim;
                    while ssstrim.len() > 0 {
                        let (packet, strim) = Packet::new(ssstrim);
                        ssstrim = strim;
                        v.push(packet);
                    }
                    
                } else {
                    let (number_value, strim) = consume(sstrim, 11);
                    sstrim = strim;
                    for _ in 0..number_value {
                        let (packet, strim) = Packet::new(sstrim);
                        sstrim = strim;
                        v.push(packet);
                    }
                }
                (Packet {
                    version,
                    payload:
                        match packet_type {
                            0 => PacketPayload::Operator(OperatorType::Sum, v),
                            1 => PacketPayload::Operator(OperatorType::Product, v),
                            2 => PacketPayload::Operator(OperatorType::Minimum, v),
                            3 => PacketPayload::Operator(OperatorType::Maximum, v),
                            5 => PacketPayload::Operator(OperatorType::GreaterThan, v),
                            6 => PacketPayload::Operator(OperatorType::LessThan, v),
                            7 => PacketPayload::Operator(OperatorType::EqualTo, v),
                            _ => panic!("Invalid packet type {}", packet_type)
                        }
                }, sstrim)
            }
        }
    }
    fn evaluate(&self) -> usize {
        match &self.payload {
            PacketPayload::LiteralValue(x) => *x,
            PacketPayload::Operator(op, operands) => {
                match op {
                    OperatorType::Sum => operands.iter().map(Packet::evaluate).sum(),
                    OperatorType::Product => operands.iter().map(Packet::evaluate).product(),
                    OperatorType::Minimum => operands.iter().map(Packet::evaluate).min().unwrap(),
                    OperatorType::Maximum => operands.iter().map(Packet::evaluate).max().unwrap(),
                    OperatorType::GreaterThan => {
                        assert_eq!(operands.len(), 2);
                        match operands[0].evaluate() > operands[1].evaluate() {
                            true => 1,
                            false => 0
                        }
                    },
                    OperatorType::LessThan => {
                        assert_eq!(operands.len(), 2);
                        match operands[0].evaluate() < operands[1].evaluate() {
                            true => 1,
                            false => 0
                        }
                    },
                    OperatorType::EqualTo => {
                        assert_eq!(operands.len(), 2);
                        match operands[0].evaluate() == operands[1].evaluate() {
                            true => 1,
                            false => 0
                        }
                    }
                }
            }
        }
    }
}

fn part_one(s: &str) -> usize {
    let mut ret = 0;
    let iter = string_to_iter(s);
    let mut q: VecDeque<&Packet> = VecDeque::new();
    let (packet, _) = Packet::new(&iter);
    q.push_back(&packet);
    loop {
        if let Some(packet) = q.pop_front() {
            ret += packet.version;
            match &packet.payload {
                PacketPayload::Operator(_, subpackets) => {
                    subpackets.iter().for_each(|x| q.push_back(x));
                },
                _ => {
                    // do nothing
                }
            }
        } else {
            break;
        }
    }
    ret
}

fn part_two(s: &str) -> usize {
    let iter = string_to_iter(s);
    let (packet, _) = Packet::new(&iter);
    packet.evaluate()
}

#[test]
fn test_parse_literal_value() {
    let iter = string_to_iter("D2FE28");
    let (packet, _) = Packet::new(&iter);
    assert_eq!(packet.version, 6);
    if let PacketPayload::LiteralValue(x) = packet.payload {
        assert_eq!(x, 2021);
    } else {
        assert!(false);
    }
}


#[test]
fn test_part_one() {
    assert_eq!(part_one("8A004A801A8002F478"), 16);
    assert_eq!(part_one("620080001611562C8802118E34"), 12);
    assert_eq!(part_one("C0015000016115A2E0802F182340"), 23);
    assert_eq!(part_one("A0016C880162017C3686B18A3D4780"), 31);
}

#[test]
fn test_part_two() {
    assert_eq!(part_two("C200B40A82"), 3);
    assert_eq!(part_two("04005AC33890"), 54);
    assert_eq!(part_two("880086C3E88112"), 7);
    assert_eq!(part_two("CE00C43D881120"), 9);
}

fn main() {
    println!("{}",part_one("20546C8802538E136091C1802689BCD7DA45948D319D1B100747A009C97696E8B4ABFCA6AB8F4F26C401964A6271C80F802D392C01CEDDCE6E5CB829802F600A00021B14E34C361006E0AC418BB2CA6800BE4599BB6A73507002A52BEEB14D201802F600849E64D3369D37C74100866785B3D0ADFD8E601E5EB9DE2366D93ECB8B040142CB8ACE07CCB5CF34CA89380410B6134CE6FEF104A2B200243396976A00401A45004313D68435DBDDDA61CE6428C01491AEBF0C7E580AE00CCC401B86514216880370EE3443D2013DF003750004361343D88800084C4C8B116A679018300740010C8571BA32080350DA0D42800043A3044189AE0174B314D76E1F3ACF3BDAE3EE7298FF134002EF9DBCD0644127E3CAE7FCBA9A80393544F9A927C973DF1A500965A5CEA94C4DDA5658B94C6C3002A798A629CF21280532BAB4F4C7271E45EE6E71D8143A9BC7948804AB94D1D6006AC200EC1E8A10C00010985316A35C3620061E641644D661A4C012993E99208FC60097802F28F528F738606008CA47205400814C89CC8890064D400AB4BE0A66F2BF253E73AE8401424A7BFB16C0037E06CE0641E0013B08010A8930CE2B980351161DC3730066274188B020054A5E16965940057895ADEB5BF56A635ADE2354191D70566273A6F5B078266008D8022200D46E8291C4401A8CF0CE33CEDE55E9F9802BA00B4BD44A5EA2D10CC00B40316800BAE1003580A6D6026F00090E50024007C9500258068850035C00A4012ED8040B400D71002AF500284009700226336CA4980471D655E25D4650888023AB00525CAE5CBA5E428600BE003993778CB4732996E9887AE3F311C291004BD37517C0041E780A7808802AF8C1C00D0CDBE4ACAD69B3B004E13BDF23CAE7368C9F62448F64546008E0034F3720192A67AD9254917454200DCE801C99ADF182575BBAACAC7F8580"));
    println!("{}",part_two("20546C8802538E136091C1802689BCD7DA45948D319D1B100747A009C97696E8B4ABFCA6AB8F4F26C401964A6271C80F802D392C01CEDDCE6E5CB829802F600A00021B14E34C361006E0AC418BB2CA6800BE4599BB6A73507002A52BEEB14D201802F600849E64D3369D37C74100866785B3D0ADFD8E601E5EB9DE2366D93ECB8B040142CB8ACE07CCB5CF34CA89380410B6134CE6FEF104A2B200243396976A00401A45004313D68435DBDDDA61CE6428C01491AEBF0C7E580AE00CCC401B86514216880370EE3443D2013DF003750004361343D88800084C4C8B116A679018300740010C8571BA32080350DA0D42800043A3044189AE0174B314D76E1F3ACF3BDAE3EE7298FF134002EF9DBCD0644127E3CAE7FCBA9A80393544F9A927C973DF1A500965A5CEA94C4DDA5658B94C6C3002A798A629CF21280532BAB4F4C7271E45EE6E71D8143A9BC7948804AB94D1D6006AC200EC1E8A10C00010985316A35C3620061E641644D661A4C012993E99208FC60097802F28F528F738606008CA47205400814C89CC8890064D400AB4BE0A66F2BF253E73AE8401424A7BFB16C0037E06CE0641E0013B08010A8930CE2B980351161DC3730066274188B020054A5E16965940057895ADEB5BF56A635ADE2354191D70566273A6F5B078266008D8022200D46E8291C4401A8CF0CE33CEDE55E9F9802BA00B4BD44A5EA2D10CC00B40316800BAE1003580A6D6026F00090E50024007C9500258068850035C00A4012ED8040B400D71002AF500284009700226336CA4980471D655E25D4650888023AB00525CAE5CBA5E428600BE003993778CB4732996E9887AE3F311C291004BD37517C0041E780A7808802AF8C1C00D0CDBE4ACAD69B3B004E13BDF23CAE7368C9F62448F64546008E0034F3720192A67AD9254917454200DCE801C99ADF182575BBAACAC7F8580"));
}
