use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::ops::Add;

use itertools::Itertools;

#[derive(Debug, PartialEq, Eq)]
enum SnailNumberElement {
    RegularNumber(u32),
    OtherPair(Box<SnailNumber>),
}

#[derive(Debug, PartialEq, Eq)]
struct SnailNumber {
    left: SnailNumberElement,
    right: SnailNumberElement,
}

impl Clone for SnailNumber {
    fn clone(&self) -> Self {
        Self {
            left: self.left.clone(),
            right: self.right.clone()
        }
    }
}
impl Clone for SnailNumberElement {
    fn clone(&self) -> Self {
        match &self {
            Self::RegularNumber(x) => Self::RegularNumber(*x),
            Self::OtherPair(x) => Self::OtherPair(x.clone())
        }
    }
}
impl SnailNumberElement {
    
    fn add_leftmost(&mut self, val: u32) {
        match self {
            Self::RegularNumber(x) => {
                *x += val;
            },
            Self::OtherPair(x) => {
                x.left.add_leftmost(val);
            }
        }
        
    }
    fn add_rightmost(&mut self, val: u32) {
        match self {
            Self::RegularNumber(x) => {
                *x += val;
            },
            Self::OtherPair(x) => {
                x.right.add_rightmost(val);
            }
        }
        
    }
    fn parse_from(strim: &str) -> (Self, &str) {
        match strim.chars().nth(0) {
            Some('[') => {
                let (number, strim) = SnailNumber::parse_from(strim);
                return (SnailNumberElement::OtherPair(Box::new(number)), strim);
            }
            Some(_) => {
                    let mut num = 0;
                    let mut chars = strim.chars();
                    let mut delta = 0;
                    loop {
                        if let Some(c) = chars.next() {
                            if c.is_ascii_digit() {
                                num = num * 10 + c.to_digit(10).unwrap();
                                delta += 1;
                            } else {
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                    return (
                    SnailNumberElement::RegularNumber(num),
                    &strim[delta..],
                )
            }
            None => {
                panic!("I'm sorry, what?");
            }
        }
    }
}

impl SnailNumber {
    fn magnitude(&self) -> u32 {
        let left_val = match &self.left {
            SnailNumberElement::RegularNumber(x) => *x,
            SnailNumberElement::OtherPair(x) => x.magnitude()
        };
        let right_val = match &self.right {
            SnailNumberElement::RegularNumber(x) => *x,
            SnailNumberElement::OtherPair(x) => x.magnitude()
        };
        3 * left_val + 2 * right_val
    }
    fn parse_from(strim: &str) -> (Self, &str) {
        let strim = &strim[1..]; // Skip [
        let (left, strim) = SnailNumberElement::parse_from(strim);
        let strim = &strim[1..]; // Skip ,
        let (right, strim) = SnailNumberElement::parse_from(strim);
        let strim = &strim[1..]; // Skip ]
        (SnailNumber { left, right }, strim)
    }
    fn create(strim: &str) -> Self {
        let (number, strim) = Self::parse_from(strim);
        assert_eq!(strim.len(), 0);
        number
    }
    fn try_explode(&mut self) -> bool {
        let (_, _, ret) = self.try_explode_impl(0);
        ret
    }
    fn try_explode_impl(&mut self, level: u32) -> (Option<u32>, Option<u32>, bool) {
        if level > 3 {
            panic!("Should not go beyond here!");
        }
        if level == 3 {
            // Explode left children.
            if let SnailNumberElement::OtherPair(x) = &mut self.left {
                if let (SnailNumberElement::RegularNumber(left_digit), SnailNumberElement::RegularNumber(right_digit)) = (&x.left, &x.right) {
                    let left_digit = *left_digit;
                    let right_digit = *right_digit;
                    self.left = SnailNumberElement::RegularNumber(0);
                    self.right.add_leftmost(right_digit);
                    return (Some(left_digit), None, true);
                } else {
                    panic!("This is too deep!");
                }
            }
            // Explode right children
            if let SnailNumberElement::OtherPair(x) = &mut self.right {
                if let (SnailNumberElement::RegularNumber(left_digit), SnailNumberElement::RegularNumber(right_digit)) = (&x.left, &x.right) {
                    let left_digit = *left_digit;
                    let right_digit = *right_digit;
                    self.right = SnailNumberElement::RegularNumber(0);
                    self.left.add_rightmost(left_digit);
                    return (None, Some(right_digit), true);
                } else {
                    panic!("This is too deep!");
                }
            } else {
                return (None, None, false);
            }
        }
        assert!(level < 3);
        if let SnailNumberElement::OtherPair(left) = &mut self.left {
            let (v1, v2, ret) = left.try_explode_impl(level + 1);
            if ret {
                if let Some(val2) = v2 {
                    self.right.add_leftmost(val2);
                }
                return (v1, None, true);
            }
        }
        if let SnailNumberElement::OtherPair(right) = &mut self.right {
            let (v1, v2, ret) = right.try_explode_impl(level + 1);
            if ret {
                if let Some(val1) = v1 {
                    self.left.add_rightmost(val1);
                }
                return (None, v2, true);
            }
        }
        (None, None, false)
    }
    fn try_split(&mut self) -> bool {
        match &mut self.left {
            SnailNumberElement::RegularNumber(x) => {
                if *x >= 10 {
                    let v = *x / 2;
                    self.left = SnailNumberElement::OtherPair(Box::new(SnailNumber {
                        left: SnailNumberElement::RegularNumber(v),
                        right: SnailNumberElement::RegularNumber(v + *x % 2),
                    }));
                    return true;
                }
            },
            SnailNumberElement::OtherPair(child) => {
                if child.try_split() {
                    return true;
                }
            }
        }
        match &mut self.right {
            SnailNumberElement::RegularNumber(x) => {
                if *x >= 10 {
                    let v = *x / 2;
                    self.right = SnailNumberElement::OtherPair(Box::new(SnailNumber {
                        left: SnailNumberElement::RegularNumber(v),
                        right: SnailNumberElement::RegularNumber(v + *x % 2),
                    }));
                    return true;
                }
            },
            SnailNumberElement::OtherPair(child) => {
                if child.try_split() {
                    return true;
                }
            }
        }
        return false;
    }
    fn reduce(&mut self) {
        loop {
            if self.try_explode() {
                continue;
            }
            if self.try_split() {
                continue;
            }
            break;
        }
    }
}

impl Add for SnailNumber {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        let mut ret = Self {
            left: SnailNumberElement::OtherPair(Box::new(self)),
            right: SnailNumberElement::OtherPair(Box::new(other)),
        };
        ret.reduce();
        ret
    }
}

#[test]
fn test_magnitude() {
    assert_eq!(SnailNumber::create("[[1,2],[[3,4],5]]").magnitude(), 143);
    assert_eq!(
        SnailNumber::create("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]").magnitude(),
        1384
    );
    assert_eq!(
        SnailNumber::create("[[[[1,1],[2,2]],[3,3]],[4,4]]").magnitude(),
        445
    );
    assert_eq!(
        SnailNumber::create("[[[[3,0],[5,3]],[4,4]],[5,5]]").magnitude(),
        791
    );
    assert_eq!(
        SnailNumber::create("[[[[5,0],[7,4]],[5,5]],[6,6]]").magnitude(),
        1137
    );
    assert_eq!(
        SnailNumber::create("[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]").magnitude(),
        3488
    );
}

#[test]
fn test_split() {
    let mut number = SnailNumber::create("[[[[0,7],4],[15,[0,13]]],[1,1]]");
    assert!(number.try_split());
    assert_eq!(number, SnailNumber::create("[[[[0,7],4],[[7,8],[0,13]]],[1,1]]"));
    assert!(number.try_split());
    assert_eq!(number, SnailNumber::create("[[[[0,7],4],[[7,8],[0,[6,7]]]],[1,1]]"));
}

#[test]
fn test_explode() {
    let mut number = SnailNumber::create("[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]");
    assert!(number.try_explode());
    assert_eq!(number, SnailNumber::create("[[[[0,7],4],[7,[[8,4],9]]],[1,1]]"));
    assert!(number.try_explode());
    assert_eq!(number, SnailNumber::create("[[[[0,7],4],[15,[0,13]]],[1,1]]"));
}
#[test]
fn test_full() {
    let left = SnailNumber::create("[[[[4,3],4],4],[7,[[8,4],9]]]");
    let right = SnailNumber::create("[1,1]");
    let result = left + right;
    assert_eq!(result, SnailNumber::create("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]"));
    assert_eq!(result.magnitude(), 1384);
}

#[test]
fn test_full_v2() {
    let result: Vec<SnailNumber> = vec![
        SnailNumber::create("[1,1]"),
        SnailNumber::create("[2,2]"),
        SnailNumber::create("[3,3]"),
        SnailNumber::create("[4,4]"),
        SnailNumber::create("[5,5]"),
        SnailNumber::create("[6,6]"),
    ];
    let result = result.into_iter().reduce(|x, y| x + y).unwrap();
    assert_eq!(result, SnailNumber::create("[[[[5,0],[7,4]],[5,5]],[6,6]]"));
}

#[test]
fn test_full_v3() {
    let result: Vec<SnailNumber> = vec![
        SnailNumber::create("[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]"),
        SnailNumber::create("[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]"),
        SnailNumber::create("[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]"),
        SnailNumber::create("[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]"),
        SnailNumber::create("[7,[5,[[3,8],[1,4]]]]"),
        SnailNumber::create("[[2,[2,2]],[8,[8,1]]]"),
        SnailNumber::create("[2,9]"),
        SnailNumber::create("[1,[[[9,3],9],[[9,0],[0,7]]]]"),
        SnailNumber::create("[[[5,[7,4]],7],1]"),
        SnailNumber::create("[[[[4,2],2],6],[8,7]]")
    ];
    let result = result.into_iter().reduce(|x, y| x + y).unwrap();
    assert_eq!(result, SnailNumber::create("[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]"));
    assert_eq!(result.magnitude(), 3488);
}
fn main() {
    let file = File::open("input.txt").unwrap();
    let reader = BufReader::new(file);
    let numbers: Vec<SnailNumber> = reader.lines()
        .map(|line| {
            if let Ok(s) = line {
                let line = s.trim();
                SnailNumber::create(line)
            } else {
                panic!("Wat");
            }
        }).collect();
    println!("{}", numbers.clone().into_iter().reduce(|a,b| a + b).unwrap().magnitude());
    let max_sum = (0..numbers.len()).cartesian_product(0..numbers.len()).map(|(a,b)| {
        if a == b {
            return 0;
        }
        return (numbers[a].clone() + numbers[b].clone()).magnitude()
    }).max().unwrap();
    println!("{}", max_sum);
}
