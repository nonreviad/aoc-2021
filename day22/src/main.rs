#![allow(dead_code)]

use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

type Interval1D = (i64, i64);
type Interval2D = (Interval1D, Interval1D);
type Interval3D = (Interval1D, Interval1D, Interval1D);

#[derive(Clone, Debug)]
enum QueryType {
    On,
    Off
}
#[derive(Clone, Debug)]
struct Query {
    query_type: QueryType,
    bounds: Interval3D
}
trait Interval {
    fn size(&self) -> i64;
    fn intersect(&self, other: &Self) -> Option<Self> where Self: Sized;
}
impl Interval for Interval3D {
    fn size(&self) -> i64 {
        (self.0.1 - self.0.0 + 1) *  (self.1.1 - self.1.0 + 1) * (self.2.1 - self.2.0 + 1)
    }
    fn intersect(&self, other: &Self) -> Option<Self> {
        let x_min = std::cmp::max(self.0.0, other.0.0);
        let x_max = std::cmp::min(self.0.1, other.0.1);
        let y_min = std::cmp::max(self.1.0, other.1.0);
        let y_max = std::cmp::min(self.1.1, other.1.1);
        let z_min = std::cmp::max(self.2.0, other.2.0);
        let z_max = std::cmp::min(self.2.1, other.2.1);
        if x_min > x_max || y_min > y_max || z_min > z_max {
            return None;
        }
        Some(((x_min, x_max), (y_min, y_max), (z_min, z_max)))
    }
}
impl Query {
    fn new(strim: &str) -> Self {
        let (on_off, strim) = strim.split_once(" ").unwrap();
        let query_type = match on_off {
            "on" => QueryType::On,
            "off" => QueryType::Off,
            _ => panic!("nope")
        };
        let mut x = (1i64, 1i64);
        let mut y = (1i64, 1i64);
        let mut z = (1i64, 1i64);
        strim.split(",").for_each(|s| {
            let (var, range) = s.split_once("=").unwrap();
            let (left, right) = range.split_once("..").unwrap();
            let left = left.parse::<i64>().unwrap();
            let right = right.parse::<i64>().unwrap();
            let range = (left, right);
            match var {
                "x" => x = range,
                "y" => y = range,
                "z" => z = range,
                _ => panic!("")
            };
        });
        Self {
            query_type,
            bounds: (x, y, z)
        }
    }
}
struct Bounded3DIntervalTree {
    bounds: Interval3D,
    width: usize,
    height: usize,
    depth: usize,
    values: Vec<bool>,
}

impl Bounded3DIntervalTree {
    fn index(&self, x: i64, y: i64, z: i64) -> usize {
        (z - self.bounds.2 .0) as usize * self.width * self.height
            + (y - self.bounds.1 .0) as usize * self.width
            + (x - self.bounds.0 .0) as usize
    }
    fn from_queries(bounds: Interval3D, queries: &[Query]) -> Self {
        let width = (bounds.0.1 - bounds.0.0 + 1) as usize;
        let height = (bounds.1.1 - bounds.1.0 + 1) as usize;
        let depth = (bounds.2.1 - bounds.2.0 + 1) as usize;
        let values: Vec<bool> = vec![false; width * height * depth];
        let mut interval_tree = Self {
            bounds,
            width,
            height,
            depth,
            values,
        };
        interval_tree.process_queries(queries);
        interval_tree
    }
    fn process_queries(&mut self, queries: &[Query]) {
        for q in queries {
            let bounds = self.bounds.intersect(&q.bounds);
            if bounds.is_none() {
                continue;
            }
            let true_bounds = bounds.unwrap();
            let val = if let QueryType::On = q.query_type {true} else  {false};
            for x in true_bounds.0.0..=true_bounds.0.1 {
                for y in true_bounds.1.0..=true_bounds.1.1 {
                    for z in true_bounds.2.0..=true_bounds.2.1 {
                        let idx = self.index(x,y,z);
                        self.values[idx] = val;
                    }
                }
            }
        }
    }
    fn count_on(&self) -> usize {
        self.values.iter().filter(|x| **x).count()
    }
}
fn include_exclude(interval: &Interval3D, ops: &[Query]) -> i64 {
    let mut ret = 0;
    for i in 0..ops.len() {
        let interval = interval.intersect(&ops[i].bounds);
        if let Some(interval) = interval {
            ret += interval.size() - include_exclude(&interval, &ops[..i]);
        }
    }
    ret
}
fn read_queries(filename: &str) -> Vec<Query> {
    let file = File::open(filename).expect("No such file");
    let reader = BufReader::new(file);
    reader.lines().into_iter().map(|line| {
        let line = line.unwrap();
        let line = line.trim();
        Query::new(&line)
    }).collect()
}
fn process_queries(queries: &Vec<Query>) -> i64 {
    let mut ret = 0;
    let queries: Vec<Query> = queries.to_vec().into_iter().rev().collect();
    for idx in 0..queries.len() {
        if let QueryType::On = queries[idx].query_type {
            ret += queries[idx].bounds.size() - include_exclude(&queries[idx].bounds, &queries[..idx]);
        }
    }
    ret
}
#[test]
fn test_part_one() {
    let queries = read_queries("input_smol.txt");
    let interval_tree = Bounded3DIntervalTree::from_queries(((-50,50), (-50, 50), (-50,50)), &queries);
    assert_eq!(interval_tree.count_on(), 39);
}

#[test]
fn test_part_two() {
    let queries = read_queries("input_less_smol.txt");
    assert_eq!(process_queries(&queries), 2758514936282235);
}
fn main() {
    let queries = read_queries("input.txt");
    let interval_tree = Bounded3DIntervalTree::from_queries(((-50,50), (-50, 50), (-50,50)), &queries);
    println!("{}", interval_tree.count_on());
    println!("{}", process_queries(&queries));
}
