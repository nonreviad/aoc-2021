fn best_y(from_x: i32, to_x: i32, from_y: i32, to_y: i32) -> (i32, i32) {
    let mut max_y = 0;
    let mut cnt = 0;
    for v0_x in 1..=to_x {
        for v0_y in to_y..=-to_y {
            let mut x = 0;
            let mut y = 0;
            let mut vx = v0_x;
            let mut vy = v0_y;
            let mut best_y_so_far = 0;
            let mut valid = false;
            loop {
                x += vx;
                y += vy;
                if x >= from_x && x <= to_x && y <= from_y && y >= to_y {
                    valid = true;
                    cnt +=1;
                    break;
                }
                if y > best_y_so_far {
                    best_y_so_far = y;
                }
                if (x < from_x || x > to_x) && vx == 0 {
                    break;
                }
                if x > to_x || y < to_y {
                    break;
                }
                vx -= vx.signum();
                vy -= 1;
            }
            if valid && best_y_so_far > max_y {
                max_y = best_y_so_far;
            }
        }
    }
    (max_y, cnt)
}

#[test]
fn part_one() {
    assert_eq!(best_y(20, 30, -5, -10).0, 45);
}
#[test]
fn part_two() {
    assert_eq!(best_y(20, 30, -5, -10).1, 112);
}
fn main() {
    let answer = best_y(70, 96, -124, -179);
    println!("{:?}", answer);
}
