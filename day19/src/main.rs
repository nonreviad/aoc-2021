use regex::Regex;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::cmp::max;
type Transform = [i32; 9];

const TRANSFORMS: [Transform; 24] = [
    [ 1, 0, 0, 0, 1, 0, 0, 0, 1],  // x, y, z
    [ 1, 0, 0, 0,-1, 0, 0, 0,-1],  // x,-y,-z
    [ 1, 0, 0, 0, 0, 1, 0,-1, 0],  // x, z,-y
    [ 1, 0, 0, 0, 0,-1, 0, 1, 0],  // x,-z, y
    [-1, 0, 0, 0, 1, 0, 0, 0,-1],  //-x, y,-z
    [-1, 0, 0, 0,-1, 0, 0, 0, 1],  //-x,-y, z
    [-1, 0, 0, 0, 0, 1, 0, 1, 0],  //-x, z, y
    [-1, 0, 0, 0, 0,-1, 0,-1, 0],  //-x,-z,-y
    [ 0, 1, 0, 1, 0, 0, 0, 0,-1],  // y, x,-z
    [ 0, 1, 0,-1, 0, 0, 0, 0, 1],  // y,-x, z
    [ 0, 1, 0, 0, 0, 1, 1, 0, 0],  // y, z, x
    [ 0, 1, 0, 0, 0,-1,-1, 0, 0],  // y,-z,-x
    [ 0,-1, 0, 1, 0, 0, 0, 0, 1],  //-y, x, z
    [ 0,-1, 0,-1, 0, 0, 0, 0,-1],  //-y,-x,-z
    [ 0,-1, 0, 0, 0, 1,-1, 0, 0],  //-y, z,-x
    [ 0,-1, 0, 0, 0,-1, 1, 0, 0],  //-y,-z, x
    [ 0, 0, 1, 1, 0, 0, 0, 1, 0],  // z, x, y
    [ 0, 0, 1,-1, 0, 0, 0,-1, 0],  // z,-x,-y
    [ 0, 0, 1, 0, 1, 0,-1, 0, 0],  // z, y,-x
    [ 0, 0, 1, 0,-1, 0, 1, 0, 0],  // z,-y, x
    [ 0, 0,-1, 1, 0, 0, 0,-1, 0],  //-z, x,-y
    [ 0, 0,-1,-1, 0, 0, 0, 1, 0],  //-z,-x, y
    [ 0, 0,-1, 0, 1, 0, 1, 0, 0],  //-z, y, x
    [ 0, 0,-1, 0,-1, 0,-1, 0, 0],  //-z,-y,-x
];
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
struct Point {
    x: i32,
    y: i32,
    z: i32,
}
impl Point {
    fn rotate(&self, transform: &Transform) -> Point {
        Point {
            x: self.x * transform[0] + self.y * transform[1] + self.z * transform[2],
            y: self.x * transform[3] + self.y * transform[4] + self.z * transform[5],
            z: self.x * transform[6] + self.y * transform[7] + self.z * transform[8],
        }
    }
    fn translate(&self, delta: &Point) -> Point {
        Point { x: self.x + delta.x, y: self.y + delta.y, z: self.z + delta.z }
    }
    fn manhattan_distance(&self, other: &Point) -> i32 {
        (self.x - other.x).abs() + (self.y - other.y).abs() + (self.z - other.z).abs()
    }

}
#[derive(Debug, Clone)]
struct Scanner {
    id: i32,
    origin: Point,
    points: Vec<Point>,
}

impl Scanner {
    fn from_lines(strim: &[String]) -> (Scanner, &[String]) {
        let id_line = strim[0].trim();
        let strim = &strim[1..];
        let r = Regex::new("--- scanner (?P<id>\\d+) ---").unwrap();
        let id_captures = r.captures(id_line).unwrap();
        let id: i32 = id_captures
            .name("id")
            .unwrap()
            .as_str()
            .parse::<i32>()
            .unwrap();
        let mut strim = strim;
        let mut points: Vec<Point> = Vec::new();
        loop {
            let line = strim[0].trim();
            if line.len() == 0 {
                strim = &strim[1..];
                break;
            }
            let numbers: Vec<i32> = line.split(",").map(|s| s.parse::<i32>().unwrap()).collect();
            points.push(Point {
                x: numbers[0],
                y: numbers[1],
                z: numbers[2],
            });
            if strim.len() == 1 {
                strim = &strim[1..];
                break;
            }
            strim = &strim[1..];
        }
        (Scanner { id, points, origin: Point { x: 0, y: 0, z: 0 } }, strim)
    }
    fn match_with(&self, other: &Scanner) -> Option<(Point,Transform,Point)> {
        let l1 = self.points.len();
        let l2 = other.points.len();
        for idx1 in 0..l1 {
            for idx2 in 0..l2 {
                for transform in TRANSFORMS {
                    let ref1 = self.points[idx1].clone();
                    let ref2 = other.points[idx2].clone();
                    let points1: HashSet<Point> = self
                        .points
                        .iter()
                        .map(|p| Point {
                            x: p.x - ref1.x,
                            y: p.y - ref1.y,
                            z: p.z - ref1.z,
                        })
                        .collect();
                    let points2: HashSet<Point> = other.points.iter().map(|p| {
                        Point {
                            x: p.x - ref2.x,
                            y: p.y - ref2.y,
                            z: p.z - ref2.z,
                        }.rotate(&transform)
                    }).collect();
                    if points1.intersection(&points2).count() >= 12 {
                        return Some((Point{x: -ref2.x, y: -ref2.y, z: -ref2.z}, transform, ref1))
                    }
                }
            }
        }
        None
    }
}
fn part_one_and_two(filename: &str) -> (usize, i32) {
    let file = File::open(filename).unwrap();
    let reader = BufReader::new(file);
    let lines: Vec<_> = reader.lines().map(|line| line.unwrap()).collect();
    let mut strim = lines.as_slice();
    let mut scanners: HashMap<i32, Scanner> = HashMap::new();
    while strim.len() > 0 {
        let (scanner, s) = Scanner::from_lines(strim);
        strim = s;
        scanners.insert(scanner.id, scanner);
    }
    let scanner_ids: Vec<i32> = scanners.keys().map(|x| *x).collect();
    let mut components: Vec<Vec<Scanner>> = Vec::new();
    let mut connected_scanners: HashSet<i32> = HashSet::new();
    
    for id in scanner_ids.iter() {
        if connected_scanners.contains(id) {
            continue;
        }
        let mut new_component: Vec<Scanner> = Vec::new();
        let mut to_check: Vec<Scanner> = Vec::new();
        to_check.push(scanners.get(id).unwrap().clone());
        connected_scanners.insert(*id);
        loop {
            if let Some(scanner1) = to_check.pop() {
                new_component.push(scanner1.clone());
                for id2 in scanner_ids.iter() {
                    if connected_scanners.contains(id2) {
                        continue;
                    }
                    let scanner2 =  scanners.get(id2).unwrap();
                    if let Some((delta2, transform, delta1)) = scanner1.match_with(scanner2) {
                        let mut scanner2 = scanner2.clone();
                        scanner2.points = scanner2.points.iter().map(|p| p.translate(&delta2).rotate(&transform).translate(&delta1)).collect();
                        scanner2.origin = delta2.rotate(&transform).translate(&delta1);
                        to_check.push(scanner2);
                        connected_scanners.insert(*id2);
                    }
                }
            } else {
                break;
            }
        }
        components.push(new_component);
    }
    let mut num_points = 0;
    let mut max_dist = 0;
    for component in components {
        let mut points: HashSet<Point> = HashSet::new();
        let num_pts_in_component = component.len();
        for i1 in 0..num_pts_in_component {
            for i2 in 0..num_pts_in_component {
                let p1 = &component[i1].origin;
                let p2 = &component[i2].origin;
                max_dist = max(max_dist, p1.manhattan_distance(p2));
            }
        }
        for scanner in component {
            points.extend(scanner.points);
        }
        num_points += points.len();
    }
    (num_points, max_dist)
}
#[test]
fn test_part_one_and_two() {
    assert_eq!(part_one_and_two("input_smol.txt"), (79, 3621));
}
fn main() {
    let (part_one, part_two) = part_one_and_two("input.txt");
    println!("{} {}", part_one, part_two);
}
