use std::io::BufReader;
use std::io::prelude::*;
use std::fs::File;

enum Direction {
    Forward(u32),
    Down(u32),
    Up(u32)
}

struct Position {
    horizontal: u32,
    depth: u32
}

struct PositionAndAim {
    horizontal: u32,
    depth: u32,
    aim: u32
}

impl From<String> for Direction {
    fn from(from_str: String) -> Self {
        let items: Vec<&str> = from_str.split(' ').collect();
        let number: u32 = match items[1].parse() {
            Ok(x) => x,
            _ => 0
        };
        match items[0] {
            "forward" => Direction::Forward(number),
            "down" => Direction::Down(number),
            "up" => Direction::Up(number),
            _ => panic!("Cannot parse {}", from_str)
        }
    }
}

impl Position {
    fn new() -> Self {
        Position{
            horizontal: 0,
            depth: 0
        }
    }
    fn step(mut self: Self, direction: &Direction) -> Self {
        match direction {
            Direction::Forward(x) => self.horizontal += x,
            Direction::Down(x)    => self.depth      += x,
            Direction::Up(x)      => self.depth      -= x,
        }
        self
    }
}

impl PositionAndAim {
    fn new() -> Self {
        PositionAndAim {
            horizontal: 0,
            depth: 0,
            aim: 0
        }
    }
    fn step(mut self: Self, direction: &Direction) -> Self {
        match direction {
            Direction::Forward(x) => { self.horizontal += x; self.depth += x * self.aim; },
            Direction::Down(x)    => self.aim += x,
            Direction::Up(x)      => self.aim -= x,
        }
        self
    }
}

fn main() -> std::io::Result<()> {
    let f = File::open("input.txt")?;
    let reader = BufReader::new(f);
    let vec: Vec<Direction> = reader
        .lines()
        .map(|line| Direction::from(line.unwrap()))
        .collect();
    // Part one
    let position: Position = vec
        .iter()
        .fold(Position::new(), |a, direction| a.step(direction));
    println!("{}", position.depth * position.horizontal);
    // Part two
    let position: PositionAndAim = vec
        .iter()
        .fold(PositionAndAim::new(), |a, direction| a.step(direction));
    println!("{}", position.depth * position.horizontal);
    Ok(())
}