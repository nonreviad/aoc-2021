use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::collections::HashSet;

#[derive(Debug, PartialEq, Eq, Hash)]
struct Dot {
    x: u32,
    y: u32
}

fn print_dots(dots: &HashSet<Dot>) {
    let mut maxx: u32 = 0;
    let mut maxy: u32 = 0;
    dots.iter().for_each(|dot| {
        if dot.x > maxx {
            maxx = dot.x;
        }
        if dot.y > maxy {
            maxy = dot.y;
        }
    });
    for y in 0..=maxy {
        for x in 0..=maxx {
            if dots.contains(&Dot{x:x, y: y}) {
                print!("#");
            } else {
                print!(".");
            }
        }
        print!("\n");
    }
}

fn parse_command(line: &str, dots: HashSet<Dot>) -> HashSet<Dot> {
    let line = line.trim();
    let lines: Vec<&str> = line.split(' ').collect();
    let line = lines[2];
    let lines: Vec<&str> = line.split('=').collect();
    let axis: &str = lines[0];
    let val: u32 = lines[1].parse::<u32>().unwrap();
    dots.iter()
        .map(|dot| -> Dot {
            match axis {
                "x" => {
                    if dot.x > val {
                        return Dot {
                            x: val - (dot.x - val),
                            y: dot.y
                        };
                    } else {
                        return Dot {
                            x: dot.x,
                            y: dot.y
                        };
                    }
                },
                "y" => {
                    if dot.y > val {
                        return Dot {
                            x: dot.x,
                            y: val - (dot.y - val)
                        };
                    } else {
                        return Dot {
                            x: dot.x,
                            y: dot.y
                        };
                    }
                },
                _ => panic!("Wrong axis")
            }
        })
        .collect()
}
fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let mut reader = BufReader::new(file);
    let mut dots: HashSet<Dot> = HashSet::new();
    loop {
        let mut line = String::new();
        let sz = reader.read_line(&mut line);
        match sz {
            Ok(1) => break,
            Ok(_) => {
                let line = line.trim();
                let mut nums = line.split(',');
                let x: u32 = nums.next().unwrap().parse::<u32>().unwrap();
                let y: u32 = nums.next().unwrap().parse::<u32>().unwrap();
                dots.insert(Dot{x:x, y:y});
            },
            _ => {
                panic!("Failed to read line")
            }
        }
    }
    let mut line = String::new();
    let _ = reader.read_line(&mut line);
    let mut dots = parse_command(&line, dots);
    println!("{}", dots.len());
    loop {
        let mut line = String::new();
        let sz = reader.read_line(&mut line);
        match sz {
            Ok(0) => {break;},
            Ok(_) => {
                dots = parse_command(&line, dots);
            },
            _ => { panic!("Error"); }
        }
    }
    print_dots(&dots);
    Ok(())
}