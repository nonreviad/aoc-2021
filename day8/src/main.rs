use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

#[derive(Debug)]
enum DigitValue {
    Known(u8),
    Unknown
}

struct SevenSegmentDigit {
    segments: Vec<char>,
    value: DigitValue
}

impl SevenSegmentDigit {
    fn new(s: &str) -> Self {
        let s = s.trim();
        let value = 
        match s.len() {
            2 => DigitValue::Known(1),
            3 => DigitValue::Known(7),
            4 => DigitValue::Known(4),
            7 => DigitValue::Known(8),
            _ => DigitValue::Unknown
        };
    
        let mut segments: Vec<char> = s.chars().collect();
        segments.sort();
        SevenSegmentDigit {
            value: value,
            segments: segments
        }
    }
}

struct SevenSegmentDisplay {
    digits: Vec<SevenSegmentDigit>,
    inputs: Vec<SevenSegmentDigit>
}

impl SevenSegmentDisplay {
    fn new(s: &str) -> Self {
        let s = s.trim();
        let mut parts = s.split('|');
        let digits: Vec<SevenSegmentDigit> = parts.next().unwrap().split(' ')
            .map(|part| SevenSegmentDigit::new(part))
            .collect();
        let inputs: Vec<SevenSegmentDigit> = parts.next().unwrap().split(' ')
            .filter(|x| x.len() > 0)
            .map(|part| SevenSegmentDigit::new(part))
            .collect();
        SevenSegmentDisplay {
            digits: digits,
            inputs: inputs
        }
    }
    fn find_digit(self: &mut Self, val: u8) -> &SevenSegmentDigit {
        for digit in self.digits.iter() {
            if let DigitValue::Known(x) = digit.value {
                if x == val {
                    return digit;
                }
            }
        }
        panic!("Wait, what?");
    }
    fn deduce(self:&mut Self) -> u32 {
        let one: Vec<char>  = self.find_digit(1).segments.clone();
        let four: Vec<char> = self.find_digit(4).segments.clone();
        let bd: Vec<char> = four.clone().into_iter().filter(|x| !one.contains(x)).collect();
        for digit in self.digits.iter_mut() {
            if let DigitValue::Unknown = digit.value {
                match digit.segments.len() {
                    5 => {
                        // 2, 3 or 5
                        // Fully intersects with 1 => 3
                        if digit.segments.contains(&one[0]) && digit.segments.contains(&one[1]) {
                            digit.value = DigitValue::Known(3);
                        } else if digit.segments.contains(&bd[0]) && digit.segments.contains(&bd[1]) {
                            digit.value = DigitValue::Known(5);
                        } else {
                            digit.value = DigitValue::Known(2);
                        }
                    },
                    6 => {
                        // 0, 6, 9
                        if digit.segments.contains(&four[0]) && digit.segments.contains(&four[1]) && digit.segments.contains(&four[2]) && digit.segments.contains(&four[3]) {
                            digit.value = DigitValue::Known(9);
                        } else if digit.segments.contains(&one[0]) && digit.segments.contains(&one[1]) {
                            digit.value = DigitValue::Known(0);
                        } else {
                            digit.value = DigitValue::Known(6);
                        }
                    },
                    _ => {}
                }
            }
        }
        for digit in self.inputs.iter_mut() {
            for known in self.digits.iter() {
                if digit.segments.len() == known.segments.len() && digit.segments.iter().all(|x| known.segments.contains(x)) {
                    if let DigitValue::Known(x) = known.value {
                        digit.value = DigitValue::Known(x);
                    }
                }
            }
        }
        let mut ret: u32 = 0;
        for digit in self.inputs.iter() {
            if let DigitValue::Known(x) = digit.value {
                ret = ret * 10 + x as u32;
            } else {
                panic!("{:?}", digit.segments);
            }      
        }
        ret
    }
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);
    let mut displays: Vec<SevenSegmentDisplay> =
        reader.lines()
        .map(|line| {
            let line = line.unwrap();
            SevenSegmentDisplay::new(&line)
        })
        .collect();
    // Part 1
    let known_digits: u32 = displays
        .iter()
        .flat_map(|display| display.inputs
                            .iter()
                            .map(|number| { 
                                if let DigitValue::Known(_) = number.value {
                                    return 1;
                                }
                                return 0;
                            }))
        .sum();
    println!("{}", known_digits);

    // Part 2
    let part2: u32 = displays.iter_mut().map(|x| x.deduce()).sum();
    println!("{}", part2);
    Ok(())
}