use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::collections::VecDeque;
use std::collections::HashSet;
struct Cavern {
    floor: Vec<u8>
}

impl Cavern {
    fn new(floor: Vec<u8>) -> Self {
        Cavern {
            floor: floor
        }
    }
    fn step(self: &mut Self) -> usize {
        let mut to_propagate: VecDeque<usize> = VecDeque::new();
        let mut flashed: HashSet<usize> = HashSet::new();
        for i in 0..100 {
            self.floor[i] += 1;
            if self.floor[i] > 9 {
                self.floor[i] = 0;
                to_propagate.push_back(i);
                flashed.insert(i);
            }
        }
        loop {
            if let Some(node) = to_propagate.pop_front() {
                let l: usize = (node / 10) + 1;
                let c: usize = (node % 10) + 1;
                for dl in 0..=2 {
                    for dc in 0..=2 {
                        let new_l = l + dl - 1;
                        let new_c = c + dc - 1;
                        if new_l >= 1 && new_c >= 1 && new_l <= 10 && new_c <= 10 {
                            let new_l = new_l - 1;
                            let new_c = new_c - 1;
                            let pos = new_l * 10 + new_c;
                            if !flashed.contains(&pos) {
                                self.floor[pos] += 1;
                                if self.floor[pos] > 9 {
                                    self.floor[pos] = 0;
                                    flashed.insert(pos);
                                    to_propagate.push_back(pos);
                                }
                            }
                        }
                    }
                }
            } else {
                break;
            }
        }
        flashed.len()
    }
}

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);
    let floor: Vec<String> = reader.lines()
        .map(|line| String::from(line.unwrap().trim()))
        .collect();
    let floor: String = floor.join("");
    let floor: Vec<u8> = floor.chars().map(|c: char| (c as u8) - b'0').collect();
    let mut cavern = Cavern::new(floor);
    let mut flashes: usize = 0;
    let mut first: usize = 0;
    for i in 0..100 {
        let steps = cavern.step();
        if steps == 100 && first == 0 {
            first = i;
        }
        flashes += steps;
    }
    println!("{}", flashes);
    let mut i = 100;
    while first == 0 {
        let steps = cavern.step();
        if steps == 100 {
            first = i;
        } else {
            i += 1;
        }
    }
    println!("{}", first + 1);
    Ok(())
}
