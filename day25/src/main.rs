use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::collections::HashMap;
#[derive(Debug, PartialEq, Eq, Hash, Clone)]
enum Direction {
    South,
    East
}
impl Direction {
    fn from_char(c: char) -> Self {
        match c {
            '>' => Self::East,
            'v' => Self::South,
            _ => panic!("Invalid direction char")
        }
    }
}
#[derive(Debug, PartialEq, Eq, Hash, Clone)]
struct Cucumber {
    position: (usize, usize),
    direction: Direction
}

impl Cucumber {
    fn next_position(&self, width: usize, height: usize) -> (usize, usize) {
        match &self.direction {
            &Direction::East => ((self.position.0 + 1) % width, self.position.1),
            &Direction::South => (self.position.0, (self.position.1 + 1) % height)
        }
    }
}

fn evolve(sea_floor: HashMap<(usize, usize), Cucumber>, width: usize, height: usize) -> (bool, HashMap<(usize, usize), Cucumber>) {
    let mut changed = false;
    let next_sea_floor: HashMap<(usize, usize),Cucumber> = sea_floor.iter().map(|(old_position, cucumber)| {
        if let Direction::South = cucumber.direction {
            return (*old_position, cucumber.clone());
        }
        let new_position = cucumber.next_position(width, height);
        if sea_floor.contains_key(&new_position) {
            return (*old_position, cucumber.clone());
        } else {
            changed = true;
            return (new_position, Cucumber {
                position: new_position,
                direction: cucumber.direction.clone()
            })
        }
    }).collect();
    let next_sea_floor: HashMap<(usize, usize),Cucumber> = next_sea_floor.iter().map(|(old_position, cucumber)| {
        if let Direction::East = cucumber.direction {
            return (*old_position, cucumber.clone());
        }
        let new_position = cucumber.next_position(width, height);
        if next_sea_floor.contains_key(&new_position) {
            return (*old_position, cucumber.clone());
        } else {
            changed = true;
            return (new_position, Cucumber {
                position: new_position,
                direction: cucumber.direction.clone()
            })
        }
    }).collect();
    (changed, next_sea_floor)
}
fn part_one(filename: &str) -> u32 {
    let file = File::open(filename).unwrap();
    let reader = BufReader::new(file);
    let mut width = 0;
    let mut height = 0;
    let sea_floor: HashMap<(usize, usize), Cucumber> = reader.lines().enumerate().flat_map(|(y, line)| {
        let line = line.unwrap();
        let line = line.trim();
        if y > height {
            height = y;
        }
        line.chars().enumerate().filter(|(_, c)| *c != '.').map(|(x, c)| {
            if x > width {
                width = x;
            }
            Cucumber {
                position: (x, y),
                direction: Direction::from_char(c)
            }
        }).collect::<Vec<_>>()
    }).map(|cucumber| (cucumber.position, cucumber)).collect();
    width += 1;
    height += 1;
    let mut sea_floor = sea_floor;
    let mut times = 0;
    loop {
        let (changed, new_floor) = evolve(sea_floor, width, height);
        sea_floor = new_floor;
        times += 1;
        if !changed {
            break;
        }
    }
    times
}
#[test]
fn test_part_one() {
    assert_eq!(part_one("input_smol.txt"), 58);
}
fn main() {
    println!("{}", part_one("input.txt"));
}
