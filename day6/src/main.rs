use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let mut reader = BufReader::new(file);
    let mut counts: Vec<i64> = vec![0;9];
    let mut line = String::new();
    let _ = reader.read_line(&mut line);
    line.split(',').for_each(|num| {
        let counter: usize = num.parse::<usize>().unwrap();
        counts[counter] += 1;
    });
    // Part 1
    for _ in 0..80 {
        let new_fish = counts[0];
        for i in 0..=7 {
            counts[i] = counts[i+1];
        }
        counts[6] += new_fish;
        counts[8] = new_fish;
    }
    let s: i64 = counts.iter().sum();
    println!("{}", s);
    // Part 2
    for _ in 80..256 {
        let new_fish = counts[0];
        for i in 0..=7 {
            counts[i] = counts[i+1];
        }
        counts[6] += new_fish;
        counts[8] = new_fish;
    }
    let s: i64 = counts.iter().sum();
    println!("{}", s);
    Ok(())
}
